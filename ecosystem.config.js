module.exports = {
  apps: [
    {
      name: 'puppet-scroll',
      script: './dist/index.js',
    },
    {
      name: 'puppet-scroll-set-ip',
      cron_restart: '* * * * *',
      autorestart: false,
      script: './dist/set_ip.js',
    },
    // {
    //   name: 'image-server',
    //   script: './dist/image_server.js',
    //   autorestart: true,
    // },
    {
      name: 'proxy-ip',
      script: './dist/update_ip.js',
      autorestart: false,
      cron_restart: '* * * * *',
    },
    // {
    //   name: 'captcha',
    //   script: './computer/captcha.js',
    //   autorestart: true,
    // },
    {
      name: 'sellapp',
      script: './dist/auto.js',
      autorestart: true,
    },
    {
      name: 'sellix',
      script: 'dist/sellix.js',
    },
    {
      name: 'ghunt',
      script: 'dist/ghuntServer.js',
    },
  ],
};
