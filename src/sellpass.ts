import puppetInstance from './Puppet';

async function main() {
  const link = 'https://timba.sellpass.io';
  const response = await puppetInstance.getSellPassURL(link);
  console.log('response', response);
}

main();
