process.setMaxListeners(0);
import server from './Express/ExpressSetup';
import puppetInstance from './Puppet';

class Main {
  constructor() {
    console.log('Test');
    this.init();
  }
  async init() {
    await puppetInstance.startBrowser();
    server.postJson('sell_app_single', puppetInstance.getSingleProductReq);
    server.postJson('', puppetInstance.getPageRequest);
    server.postJson('sell_pass', puppetInstance.getSellPass);
  }
}

async function start() {
  new Main();
}

start();
