import fs from 'fs';
import { getAtShop } from './AtShop/getAtShop';
async function main() {
  const store = process.argv[2];
  const res = await getAtShop(store);
  const path = `atshop-${store}.json`;
  fs.writeFileSync(path, JSON.stringify(res));
  process.exit(1);
}

main();
