interface SellAppGroups {
  type: string;
  value: Value;
  url: string;
  products?: SellAppProductApi[];
}

interface SellAppProductApi {
  id: number;
  title: string;
  slug: string;
  description: string;
  visibility: string;
  images: Image[];
  section_id: number;
  section_order: number;
  order: number;
  created_at: string;
  store_id: number;
  total_stock: null | number;
  prices: Price[];
  variants_count: number;
  min_price?: string;
  max_price?: string;
  badge: Badge;
  url: string;
  image_urls: string[];
  price?: string;
}

interface Value {
  id: number;
  title: string;
  order: number;
  image?: Image;
  unlisted?: boolean;
  created_at: string;
  updated_at?: string;
  store_id: number;
  section_id: number;
  section_order: number;
  products_count?: number;
  prices: Price[];
  min_price?: string;
  max_price?: string;
  image_url?: string;
  price?: string;
  slug?: string;
  description?: string;
  visibility?: string;
  images?: Image[];
  total_stock?: null | number;
  variants_count?: number;
  badge?: Badge;
  url?: string;
  image_urls?: string[];
}

interface Badge {
  text: null;
  color: null;
}

interface Price {
  price: number;
  currency: string;
}

interface Image {
  path: string;
  metadata: Metadata;
}

interface Metadata {
  size: number;
  filename: string;
  extension: string;
  mime_type: string;
}

export { SellAppGroups, SellAppProductApi };
