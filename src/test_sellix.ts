import puppetInstance from './Puppet';

async function main() {
  let link = process.argv[3];
  const proxy_n = parseInt(process.argv[4]);
  if (!link) link = 'https://flamurmart.mysellix.io/';
  console.log('link', link);
  const res = await puppetInstance.getSellixApi(link, proxy_n);
  console.log('Response', res);
}

main();
