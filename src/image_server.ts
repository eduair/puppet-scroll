import cors from 'cors';
import express, { Request, Response } from 'express';
import path from 'path';
import util from 'util';
const exec = util.promisify(require('child_process').exec);
const app = express();

app.use(cors());
app.set('json spaces', 0);
app.use(express.json());
app.set('trust proxy', true);
app.use(express.urlencoded({ extended: false }));

app.get('/screenshot', async (req: Request, res: Response) => {
  const { stdout, stderr } = await exec('python computer/screenshot.py', {
    windowsHide: true,
  });
  console.log('Response', stdout);
  const pathFile = path.join(__dirname, '..', 'screenshot.png');
  console.log('pathFile', pathFile);
  res.sendFile(pathFile, (err) => {
    if (err) {
      console.error(`Error serving image: ${err.message}`);
      res.status(404).send('Image not found');
    }
  });
});

const port = 2345;
app.listen(port, () => {
  console.log(`Screenshot App listening at http://localhost:${port}`);
});
