import { recaptcha, serverConfig } from '../config/config';
import Express from './Express';

const server = new Express(serverConfig.port, '/', recaptcha);
export default server;
