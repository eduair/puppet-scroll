import cors from 'cors';
import express, { Request, Response } from 'express';
// @ts-ignore
import queue from 'express-queue';
import { RecaptchaV2 } from 'express-recaptcha';
import { ValidationChain, validationResult } from 'express-validator';
import * as http from 'http';
import { bError } from '../General/Utils';
import logs from '../Logs';
import { maxInstances } from '../config/config';
import { FunctionExpress } from './Express.interface';
import middleware from './Middleware';

class Express {
  private port: number;
  private app: express.Application = express();
  private baseUrl: string;
  private recaptcha: RecaptchaV2 | any;

  constructor(
    port: number,
    baseUrl: string,
    recaptchaKey: { siteKey: string; secretKey: string } = {
      siteKey: '',
      secretKey: '',
    },
  ) {
    this.baseUrl = baseUrl;
    this.port = port;
    if (recaptchaKey.siteKey) {
      this.recaptcha = new RecaptchaV2(
        recaptchaKey.siteKey,
        recaptchaKey.secretKey,
      );
    }
    //const origin = 'https://shellix.xyz';
    const origin = process.env.origin;
    //const options = { credentials: true, origin: [origin, 'http://localhost:3111'] };
    this.app.use(cors());

    this.app.use(express.json({ limit: '50mb' }));
    this.app.set('trust proxy', true);
    this.app.use(express.urlencoded({ extended: false }));
    this.start();
    this.app.use(middleware.verifyRequest);
  }

  public useCache() {
    // const cacheMiddleware = new ExpressCache(
    //   cacheManager.caching({
    //     store: 'memory',
    //     max: 100000,
    //     ttl: 240,
    //   }),
    //   {
    //     hydrate: (req, res, data, cb) => {
    //       // Parse as JSON first
    //       try {
    //         const j = JSON.parse(data)
    //         res.contentType('application/json')
    //         return res.json(j)
    //       } catch (err) {
    //         console.error(err)
    //       }
    //       const f: any = fileType
    //       const guess = f(data.slice(0, 4101))
    //       if (guess) {
    //         res.contentType(guess.mime)
    //       }
    //       return data
    //     },
    //     getCacheKey: (req: Request) => {
    //       const j = req.body ? JSON.stringify(req.body) : ''
    //       const q = req.query ? JSON.stringify(req.query) : ''
    //       const url = req.url
    //       const data = `${j}${q}${url}`
    //       return crypto.createHash('md5').update(data).digest('hex')
    //     },
    //   }
    // )
    // cacheMiddleware.attach(this.app)
  }

  public getApp(): express.Application {
    return this.app;
  }

  public getRecaptchaMiddleWare() {
    if (this.recaptcha) return this.recaptcha.middleware.verify;
  }

  public getJson(requestUrl: string, f: FunctionExpress): void {
    this.app.get(
      `${this.baseUrl}${requestUrl}`,
      async (req: Request, res: Response) => {
        const result: any = await f(req, res).catch((e) => {
          return bError(e.message);
        });
        res.json(result);
      },
    );
  }

  public get(requestUrl: string, f: any): void {
    this.app.get(
      `${this.baseUrl}${requestUrl}`,
      async (req: Request, res: Response) => {
        f(req, res).catch(() => {
          res.status(404);
          res.end();
        });
      },
    );
  }

  public confirmPull(requestUrl: string, f: FunctionExpress): void {
    this.app.post(
      `${this.baseUrl}${requestUrl}`,
      async (req: Request, res: Response) => {
        res.json({ received: true });
        const result: any = await f(req);
      },
    );
  }

  public postJsonAlt2(
    requestUrl: string,
    f: FunctionExpress,
    validation: ValidationChain[] = [],
  ): void {
    this.app.post(
      `${this.baseUrl}${requestUrl}`,
      validation,
      async (req: Request, res: Response) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.json({ error: errors.array() });
        return f(req, res);
      },
    );
  }

  public postJsonAlt(
    requestUrl: string,
    f: FunctionExpress,
    validation: ValidationChain[] = [],
  ): void {
    this.app.post(
      `${this.baseUrl}${requestUrl}`,
      queue({ activeLimit: maxInstances, queuedLimit: 10 }),
      validation,
      async (req: Request, res: Response) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.json({ error: errors.array() });
        return f(req, res);
      },
    );
  }

  public postJson(
    requestUrl: string,
    f: FunctionExpress,
    validation: ValidationChain[] = [],
  ): void {
    this.app.post(
      `${this.baseUrl}${requestUrl}`,
      queue({ activeLimit: maxInstances, queuedLimit: 10 }),
      validation,
      async (req: Request, res: Response) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) return res.json({ error: errors.array() });
        const result: any = await f(req, res).catch((e) => {
          console.error(e);
          return bError(e.message);
        });
        res.json(result);
      },
    );
  }

  public postJsonRecaptcha(
    requestUrl: string,
    f: FunctionExpress,
    validation: ValidationChain[] = [],
  ): void {
    this.app.post(
      `${this.baseUrl}${requestUrl}`,
      this.recaptcha.middleware.verify,
      async (req: Request, res: Response) => {
        if (req['recaptcha'] && !req['recaptcha']['error']) {
          const errors = validationResult(req);
          if (!errors.isEmpty()) return { error: errors.array() };
          const result: any = await f(req).catch((e) => {
            return bError(e.message);
          });
          res.json(result);
        } else {
          res.json({ error: 'Bad recaptcha' });
        }
      },
    );
  }

  async start() {
    const server = http.createServer(this.app);
    server.listen(this.port, () => {
      logs.success('Express server listening on port ' + this.port);
    });
    server.on('error', function (e) {
      logs.error(`Not connected to express ${e.message}`);
    });
    server.setTimeout(50000000);
  }
}

export default Express;
