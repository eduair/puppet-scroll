import { exec } from 'child_process';
import dotenv from 'dotenv';
import { Request, Response } from 'express';
import fs from 'fs';
import util from 'node:util';
dotenv.config();
const execPromise = util.promisify(exec);

export const GHunt = async (email: string) => {
  let route = 'C:\\Users\\airau\\Desktop\\ghunt-php-api\\main.py';
  if (process.env.ghuntRoute) {
    route = process.env.ghuntRoute;
  }
  const command = `set PYTHONIOENCODING=utf-8 && set PYTHONLEGACYWINDOWSSTDIO=utf-8 && python ${route} email <email> --json ghunt/<email>.json`;
  const cmd = command.replace(/<email>/g, email);
  await execPromise(cmd);
  const filePath = `ghunt/${email}.json`;
  const fileData = fs.readFileSync(filePath, 'utf-8');
  let json = JSON.parse(fileData);
  fs.unlinkSync(filePath);
  return json;
};

export const GHuntReqPing = (req: Request, res: Response): any => {
  return res.json({ status: 'ok' });
};

export const GHuntReq = async (req: Request, res: Response): Promise<any> => {
  try {
    const email = req.body.email;
    const json = await GHunt(email);
    return res.json(json);
  } catch (e) {
    return res.json({ error: e.message });
  }
};
