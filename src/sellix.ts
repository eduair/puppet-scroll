process.setMaxListeners(0);
import { recaptcha } from './config/config';
import Express from './Express/Express';
import { gemini } from './Gemini/Gemini';
import { GHuntReq, GHuntReqPing } from './Ghunt';
import puppetInstance from './Puppet';

const server = new Express(8720, '/', recaptcha);
class Main {
  constructor() {
    console.log('Sellix Server');
    this.init();
  }
  init() {
    server.postJsonAlt('sellix', puppetInstance.getSellix);
    server.postJsonAlt('atshop', puppetInstance.getAtshop);
    server.postJson('gemini', gemini.req);
    server.postJsonAlt('ghunt', GHuntReq);
    server.postJsonAlt('ghunt/ping', GHuntReqPing);
  }
}

async function start() {
  new Main();
}

start();
