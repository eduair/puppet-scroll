import axios, { AxiosInstance } from 'axios';
import axiosRetry from 'axios-retry';
import { sleep } from './General/Utils';
import { EntryI, SellAppShop } from './SellApp.interface';

class SellApp {
  axios: AxiosInstance;
  constructor() {
    this.axios = axios.create({
      baseURL: 'http://104.234.204.107:3499/',
      headers: {
        dev: 'shellix',
      },
    });
    axiosRetry(this.axios, { retries: 3 });
  }
  buildLink(data: EntryI) {
    return `https://${data.user}.sell.app`;
  }
  async getStore() {
    try {
      const data: EntryI = await this.axios
        .get('sell_app/last')
        .then((res) => res.data);
      return { entry: data, url: this.buildLink(data) };
    } catch (e) {
      await sleep(1000);
      return this.getStore();
    }
  }
  async uploadStore(entry: EntryI, shop: SellAppShop) {
    try {
      const data = await this.axios
        .post('sell_app/update', { entry, shop })
        .then((res) => res.data);
      return data;
    } catch (e) {
      await sleep(1000);
      return this.uploadStore(entry, shop);
    }
  }
}

export const sellApp = new SellApp();
