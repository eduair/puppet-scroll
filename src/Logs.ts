import chalk from 'chalk';

const logs = {
  info: (toLog: string) => {
    console.log(chalk.blue(toLog));
  },
  success: (toLog: string) => {
    console.log(chalk.green(toLog));
  },
  error: (toLog: string) => {
    console.log(chalk.red(toLog));
  },
};

export default logs;
