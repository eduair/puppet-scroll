import puppetInstance from './Puppet';
import { sellApp } from './SellApp';
import { SellAppShop } from './SellApp.interface';

async function main() {
  async function syncLast() {
    try {
      const timer = setTimeout(() => {
        console.log('Timeout, restart process');
        process.exit(1);
      }, 60000);
      const { entry, url } = await sellApp.getStore();
      console.log('Data', entry, url);
      // 5 minutes to try to obtain the data
      const response: SellAppShop = await puppetInstance.getSellAppPage(
        url,
        false,
        '',
        timer,
      );
      if ((response as any).status !== 500) {
        console.log('Valid page', url);
        response.username = entry.user;
        console.log('To upload', response);
        const uploadResponse = await sellApp.uploadStore(entry, response);
        console.log(uploadResponse);
      } else {
        console.log('Error getting page info', response);
        process.exit(1);
      }
    } catch (e) {
      console.error(e);
      process.exit(1);
    }
    setTimeout(() => {
      syncLast();
    }, 1500);
  }
  syncLast();
}

main();
