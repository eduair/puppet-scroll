interface EntryI {
  id: string;
  user: string;
  type: string;
  creation_date: string;
  modification_date: string;
  last_update: string;
  status: number;
}

interface SellAppShop {
  custom_domain: string;
  groups: {
    [key: string]: string[];
  };
  avatar: string;
  username: string;
  link: string;
  products: SellAppProduct[];
  discord_link: string;
  telegram_link: string;
  feedback: {
    positive: number;
    neutral: number;
    negative: number;
  };
  message: string;
}

interface SellAppFeedback {
  product_id: string;
  message: string;
  reply: string;
  score: number;
  date_ago: string;
  created_at: string;
}

interface SellAppProduct {
  link: string;
  name: string;
  uniqid: string;
  gateways: string[];
  image: string;
  quantity_min: number;
  quantity_max: number;
  description: string;
  currency: string;
  price: number;
  stock: number;
  feedbacks: SellAppFeedback[];
}

export { EntryI, SellAppProduct, SellAppShop, SellAppFeedback };
