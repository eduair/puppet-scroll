import axios from 'axios';

const proxies = `http://31.40.224.32:8800
http://212.115.61.187:8800
http://69.147.248.208:8800
http://154.12.117.129:8800
http://69.147.248.41:8800
http://154.26.165.4:8800
http://212.115.61.81:8800
http://69.147.248.6:8800
http://154.26.165.129:8800
http://212.115.61.238:8800
http://31.40.224.186:8800
http://212.115.61.241:8800
http://31.40.224.6:8800
http://154.26.165.68:8800
http://154.12.117.103:8800
http://108.62.124.116:8800
http://154.12.117.67:8800
http://154.26.165.57:8800
http://154.12.117.163:8800
http://108.62.124.104:8800
http://31.40.224.152:8800
http://69.147.248.19:8800
http://31.40.224.7:8800
http://108.62.124.238:8800
http://108.62.124.137:8800
http://94.229.71.37:8800
http://94.229.71.17:8800
http://192.126.161.217:8800
http://192.126.161.100:8800
http://192.126.161.211:8800
http://104.140.209.93:8800
http://192.126.161.187:8800
http://104.140.209.178:8800
http://173.234.181.233:8800
http://173.234.181.209:8800`
  .split('\n')
  .map((el) => el.trim())
  .filter((el) => el);

function extractHostFromProxy(proxyUrl) {
  try {
    const url = new URL(proxyUrl);
    return url.hostname;
  } catch (error) {
    console.error('Invalid URL:', error);
    return null;
  }
}

async function main() {
  let newProxies = [];
  let idx = 0;
  for (let proxy of proxies) {
    const response = await axios
      .get('https://api.ipify.org?format=json', {
        proxy: {
          protocol: 'http',
          port: 8800,
          host: extractHostFromProxy(proxy),
        },
      })
      .then((res) => res.data)
      .catch((e) => {
        return null;
      });
    console.log(idx, proxies.length, proxy, response);
    if (response) {
      newProxies.push(proxy);
    }
    idx++;
  }
  console.log(newProxies.join('\n'));
}

main();
