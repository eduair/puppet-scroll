const getIP = require('external-ip')();
import axios from 'axios';

async function getIp() {
  const url = 'https://api.ipify.org/?format=json';
  const ip = await axios
    .get(url)
    .then((response) => response.data.ip as string);
  return ip;
}

const setIp = async () => {
  const ip = await getIp();
  console.log(ip);
  const res = await axios
    .post('http://104.234.204.107:3500/dead_air/set_ip', { ip })
    .then((res) => res.data);
  const res2 = await axios
    .post('http://83.147.54.179:8082/set_ip', { ip })
    .then((res) => res.data)
    .catch((e) => console.error(e));
  console.log('ip response', res, res2);
  setTimeout(() => {
    setIp();
  }, 5000);
};

setIp();
