interface AtShop_category {
  msg: string;
  collection: string;
  id: string;
  fields: Fields;
}

interface Fields {
  color: string;
  dynamicSize: boolean;
  gradient: boolean;
  position: number;
  shopId: string;
  title: string;
}

export { AtShop_category };
