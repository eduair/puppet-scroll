interface AtShopFeedback {
  msg: string;
  collection: string;
  id: string;
  fields: Fields;
}

interface Fields {
  createdAt: CreatedAt;
  feedback: Feedback;
  productId: string;
  updatedAt: CreatedAt;
}

interface Feedback {
  message: string;
  rating: string;
}

interface CreatedAt {
  $date: number;
}

export { AtShopFeedback };
