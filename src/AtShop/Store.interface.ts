enum StoreErrors {
  notFound = 'Shop not found',
  invalidLink = 'Invalid Shop Link',
  notAvailable = 'Shop not available',
}

interface FinalProductCategory {
  id: string;
  title: string;
}

interface Promo {
  expiration_date: Date;
  background_color: string;
  glow_effect: string;
}

interface EntryStore {
  id: string;
  user: string;
  type: string;
  creation_date: Date;
  last_update: Date;
}

interface CategoriesFinal {
  title: string;
  products: string[];
  id: string;
}

interface Promotion {
  expiration_date: Date;
  background_color: string;
  glow_effect: string;
}

interface FinalProduct {
  id: string;
  link: string;
  tags?: string[];
  tagsAnalytic?: string[];
  slug?: string;
  priceUSD?: number;
  priceUSD_store?: number;
  price: number;
  currency: string;
  title: string;
  image_link: string;
  description: string;
  feedbacks: GeneralFeedback;
  rating: number;
  int_rating: number;
  quantity_min: number;
  quantity_max: number;
  gateways: string[];
  stock: number;
  type: string;
  created_at: Date;
  updated_at: Date;
  last_update: Date;
  date?: Date;
  priceMinPurchase: number;
  priceMinPurchaseUSD?: number;
  promoted: boolean;
  product_language?: string;
  categories: FinalProductCategory[];
  promotion?: Promotion;
  porn: boolean;
  disabled?: boolean;
  sold_count: number;
  isInGroup?: boolean;
  random_n: number;
}

enum FinalProductUpdateType {
  new_product = 0,
  up_stock = 1,
  down_stock = 2,
  up_price = 3,
  down_price = 4,
}

interface FinalProductUpdate {
  title: string;
  link: string;
  stock: number;
  priceUSD: number;
  price: number;
  currency: string;
  type?: [FinalProductUpdateType];
}

interface FinalFeedback {
  message: string;
  product_id: string | number;
  product?: FinalProduct;
  response: string;
  score: number;
  sentimentScore: number;
  created_at: Date;
  updated_at: Date;
  feedback_language?: string;
}

interface Trustedchecks {
  identity_verified: boolean;
  sales_last14days_metric: string;
  feedback_appealed: number;
  feedback_appeal_ratio: number;
  feedback_score: number;
  no_chargeback_gateways: boolean;
  trusted_score: number;
}

interface FinalStore {
  promotion?: any;
  glow_effect?: string;
  background_color?: string;
  promotion_expiration?: Date;
  custom_domain?: string;
  alt_domain?: string;
  alt_username?: string;
  shop_name?: string;
  tagline?: string;
  link: string;
  discord_working?: boolean;
  telegram_working?: boolean;
  riskScore?: number;
  discordInfo?: any;
  telegramInfo?: any;
  verified: boolean;
  message: string;
  terms_of_service: string;
  shop: string;
  username: string;
  avatar_link: string;
  discord_link: string;
  rating: number;
  int_rating: number;
  twitter_link: string;
  instagram_link: string;
  currency: string;
  facebook_link: string;
  telegram_link: string;
  youtube_link: string;
  reddit_link: string;
  tiktok_link: string;
  telegram_tag: string[];
  discord_tag: string[];
  email: string[];
  total_products: number;
  total_groups: number;
  categories: FinalCategory[];
  feedback: GeneralFeedback;
  products: FinalProduct[];
  feedbacks: FinalFeedback[];
  online: {
    state: boolean;
    ago: string;
    dateTime?: Date;
  };
  subscription: string;
  last_update: Date;
  terminated?: boolean;
  redirection?: string;
  products_sold_count: number;
  marketplace_verified: boolean;
  trusted_checks?: Trustedchecks;
  product_ranking?: number;
  shop_ranking?: number;
  gateways: string[];
}

enum StoresEnum {
  Sellix = 'sellix',
  SellApp = 'sell_app',
  Shoppy = 'shoppy',
  Selly = 'selly',
  AtShop = 'atshop',
  AutoBuy = 'autobuy',
  SellPass = 'sell_pass',
  Invicta = 'invicta',
  Sellup = 'sellup',
}

interface DisableI {
  disable: boolean;
}

interface FinalCategory {
  title: string;
  products: any[];
  id: string;
}

interface EntryPromoteProduct {
  id: number;
  token: string;
  product_id: string;
  store: string;
  creation_date: Date;
  store_id: string;
}

interface EntryPromotions {
  product_link: string;
  store_link: string;
  token: string;
  expiration_date: Date;
  background_color: string;
  glow_effect: string;
}
interface GeneralFeedback {
  negative: number;
  neutral: number;
  positive: number;
}

interface ErrorHandler {
  error: any;
  success: any;
}

interface EntryI {
  id: string;
  user: string;
  type: string;
  creation_date: string;
  modification_date: string;
  last_update: string;
  status: number;
}

interface TokenObj {
  key: string;
  duration: number;
  plan: string;
}

interface EntrySPromoted {
  store: string;
  user: string;
}

export {
  DisableI,
  EntryI,
  EntryPromoteProduct,
  EntryPromotions,
  EntrySPromoted,
  EntryStore,
  ErrorHandler,
  FinalCategory,
  FinalFeedback,
  FinalProduct,
  FinalProductCategory,
  FinalProductUpdate,
  FinalStore,
  GeneralFeedback,
  Promo,
  StoreErrors,
  StoresEnum,
  TokenObj
};

