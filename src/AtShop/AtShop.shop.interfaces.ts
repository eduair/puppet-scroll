interface AtShop_shop {
  msg: string;
  collection: string;
  id: string;
  store: string;
  fields: Fields;
}

interface Fields {
  companyName: string;
  currency: string;
  domain: string;
  feedback: Feedback;
  name: string;
  style: Style;
  tagline: string;
  widgets: Widgets;
}

interface Widgets {
  discord: string;
  crisp: string;
  analytics: string;
  telegram: string;
}

interface Style {
  name: string;
  navBar: boolean;
  hideBranding: boolean;
  hideProductRatings: boolean;
  logoUri: string;
  logoIconUri: string;
  backgroundImage: string;
  landingBox: boolean;
}

interface Feedback {
  count: number;
  score: number;
}

export { AtShop_shop };
