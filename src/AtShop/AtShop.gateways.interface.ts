interface AtShop_gateway {
  msg: string;
  collection: string;
  id: string;
  fields: Fields;
}

interface Fields {
  email: string;
  enabled: boolean;
  multiplier: number;
  name: string;
  shopId: string;
}

export { AtShop_gateway };
