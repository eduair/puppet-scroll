import { AtShop_category } from './AtShop.categories.interfaces';
import { AtShopFeedback } from './AtShop.feedback.interface';
import { AtShop_gateway } from './AtShop.gateways.interface';
import { AtShop_group } from './AtShop.group.interfaces';
import { AtShop_shop } from './AtShop.shop.interfaces';
import { FinalFeedback } from './Store.interface';


interface AtShopProduct {
  msg: string;
  collection: string;
  id: string;
  fields: Fields;
}

interface Fields {
  category: string;
  description: string;
  displayDescription: boolean;
  feedback: Feedback;
  hidden: boolean;
  icon: string;
  image_url: string;
  maxDisplayedStock: number;
  maxQuantity: number;
  minQuantity: number;
  name: string;
  notForSale: boolean;
  paymentMethods: any[];
  preventDuplicates: boolean;
  priority: number;
  purchaseNotes: string;
  requireShipping: string;
  shopId: string;
  stockCount: number;
  style: string;
  useOrderIdAsItemName: boolean;
  value: number;
}

interface Feedback {
  count: number;
  score: number;
}

enum AtShopCollections {
  kadira_settings = 'kadira_settings',
  shops = 'shops',
  shop_products = 'shop.products',
  shop_order_feedback = 'shop.order.feedback',
  shop_product_groups = 'shop.products.group',
  shop_categories = 'shop.categories',
  shop_gateways = 'shop.gateways',
}

interface AtShopFinal {
  shop: AtShop_shop;
  groups: AtShop_group[];
  categories: AtShop_category[];
  products: AtShopProduct[];
  feedbacks: AtShopFeedback[];
  finalFeedBacks?: FinalFeedback[];
  gateways: AtShop_gateway[];
}

export { AtShopCollections, AtShopFinal, AtShopProduct };

