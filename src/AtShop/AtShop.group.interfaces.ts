interface AtShop_group {
  msg: string;
  collection: string;
  id: string;
  fields: AtShop_group_field;
}

interface AtShop_group_field {
  category: string;
  createdAt: CreatedAt;
  description: string;
  displayDescription: boolean;
  icon: string;
  image_url: string;
  maxDisplayedStock: number;
  name: string;
  priority: number;
  productIds: string[];
  shopId: string;
  style: string;
  type: string;
  updatedAt: CreatedAt;
}

interface CreatedAt {
  $date: number;
}

export { AtShop_group };
