import WebSocket from 'ws';
import { bError, makeid, sleep } from '../General/Utils';
import { AtShop_category } from './AtShop.categories.interfaces';
import { AtShopFeedback } from './AtShop.feedback.interface';
import { AtShop_gateway } from './AtShop.gateways.interface';
import { AtShop_group } from './AtShop.group.interfaces';
import {
  AtShopCollections,
  AtShopFinal,
  AtShopProduct,
} from './AtShop.interfaces';
import { AtShop_shop } from './AtShop.shop.interfaces';

let full_aws: WebSocket[] = [];

const clearAllWs = () => {
  for (const ws of full_aws) {
    try {
      ws.close();
    } catch (e) {}
  }
  full_aws = [];
};

export const isValidAtShop = async (store: string): Promise<boolean> => {
  clearAllWs(); // Clear previous connection to avoid issues
  return new Promise((resolve) => {
    const url = `wss://ddp.atshop.io/sockjs/380/vfqr846e/websocket`;
    const ws = new WebSocket(url, {
      headers: {
        'User-Agent':
          'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36',
      },
      origin: `https://${store}.atshop.io`,
    });
    full_aws.push(ws);
    ws.on('open', function open() {
      console.log('opened');
    });

    const allMessages = [
      '["{\\"msg\\":\\"connect\\",\\"version\\":\\"1\\",\\"support\\":[\\"1\\",\\"pre2\\",\\"pre1\\"]}"]',
      `["{\\"msg\\":\\"method\\",\\"method\\":\\"shop.analytics\\",\\"params\\":[\\"${store}.atshop.io\\"],\\"id\\":\\"1\\"}"]`,
    ];

    ws.on('message', async function incoming(data: string) {
      if (Buffer.isBuffer(data)) {
        data = data.toString('utf8'); // Convert buffer to string
      }
      if (data == 'o' || data == 'h') {
        console.log('sending opening message');
        for (const opMessage of allMessages) {
          ws.send(opMessage);
        }
      } else {
        let newData = (data as string).replace('a', '');
        newData = JSON.parse(newData);
        if (newData.length) {
          const dataF = JSON.parse(newData);
          if (dataF.msg && dataF.msg === 'ping') {
            ws.send('["{\\"msg\\":\\"pong\\"}"]');
            ws.send('["{\\"msg\\":\\"ping\\"}"]');
          }
          if (dataF.msg === 'result') {
            if (dataF.error) {
              ws.close();
              return resolve(false);
            } else {
              ws.close();
              return resolve(true);
            }
          }
        }
      }
    });
  });
};

const getAtShop = async (store: string, att = 0): Promise<AtShopFinal> => {
  let timeout = null;
  let breakTimeout = null;
  let result: any = await new Promise((resolve, reject) => {
    try {
      const timeoutCancelMax = setTimeout(() => {
        ws.close();
        if (breakTimeout) {
          clearTimeout(breakTimeout);
          breakTimeout = null;
        }
        if (timeout) {
          clearTimeout(timeout);
          timeout = null;
        }
        return reject({ message: 'timeout' });
      }, 60 * 15 * 1000);
      const products: AtShopProduct[] = [];
      let idShop = '';
      const ids = [];
      let shop: AtShop_shop;
      let maxReadyCount = 5;
      const charsRS = 'yBukQdvKdTBQJBbGy'.length;
      const categories: AtShop_category[] = [];
      const groups: AtShop_group[] = [];
      const gateways: AtShop_gateway[] = [];
      const feedbacks: AtShopFeedback[] = [];
      const ws = new WebSocket(
        `wss://ddp.atshop.io/sockjs/899/0tlh50sr/websocket`,
        {
          headers: {
            //Cookie: '',
            'User-Agent':
              'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36',
          },
          origin: `https://${store}.atshop.io`,
        }
      );
      ws.on('open', function open() {
        console.log('opened');
      });

      let readyCount = 0;

      const allMessages = [
        '["{\\"msg\\":\\"connect\\",\\"version\\":\\"1\\",\\"support\\":[\\"1\\",\\"pre2\\",\\"pre1\\"]}"]',
        `["{\\"msg\\":\\"method\\",\\"method\\":\\"shop.analytics\\",\\"params\\":[\\"${store}.atshop.io\\"],\\"id\\":\\"1\\"}"]`,
        //`["{\\"msg\\":\\"sub\\",\\"id\\":\\"${makeid(charsRS, ids)}\\",\\"name\\":\\"meteor.loginServiceConfiguration\\",\\"params\\":[]}"]`,
        //`["{\\"msg\\":\\"sub\\",\\"id\\":\\"${makeid(charsRS, ids)}\\",\\"name\\":\\"meteor_autoupdate_clientVersions\\",\\"params\\":[]}"]`,
        `["{\\"msg\\":\\"sub\\",\\"id\\":\\"${makeid(
          charsRS,
          ids
        )}\\",\\"name\\":\\"shops\\",\\"params\\":[\\"${store}\\"]}"]`,
      ];

      ws.onerror = function (event) {
        reject('error issue with stocket');
        ws.close();
      };
      let msgReceived = false;
      ws.on('message', async function incoming(data: string) {
        if (Buffer.isBuffer(data)) {
          data = data.toString('utf8'); // Convert buffer to string
        }
        if (!msgReceived) msgReceived = true;
        if (breakTimeout) {
          clearTimeout(breakTimeout);
          breakTimeout = null;
        }
        if (timeout) {
          clearTimeout(timeout);
          timeout = null;
        }
        if (data == 'o' || data == 'h') {
          console.log('sending opening message');
          for (const opMessage of allMessages) {
            ws.send(opMessage);
            await sleep(400);
          }
        } else {
          console.log("data", data);
          let newData = (data as string).replace('a', '');
          newData = JSON.parse(newData);
          if (newData.length) {
            const dataF = JSON.parse(newData);
            if (dataF.msg && dataF.msg === 'ping') {
              ws.send('["{\\"msg\\":\\"pong\\"}"]');
              ws.send('["{\\"msg\\":\\"ping\\"}"]');
            }
            if (dataF.msg === 'result') {
              if (dataF.id === '2' && dataF.result == true) {
                ws.close();
                return reject({ message: 'not_found' });
              } else if (dataF.id === '2') {
                readyCount++;
              }
              if (dataF.error) {
                ws.close();
                return reject({ message: 'not_found' });
              }
            } else if (dataF.msg === 'ready') {
              readyCount++;
              console.log('Count', readyCount, maxReadyCount);
              if (maxReadyCount && readyCount >= maxReadyCount) {
                ws.close();
                if (timeout) clearTimeout(timeout);
                if (timeoutCancelMax) clearTimeout(timeoutCancelMax);
                return resolve({
                  shop,
                  products,
                  feedbacks,
                  groups,
                  categories,
                  gateways,
                });
              }
            } else if (dataF.msg && dataF.msg == 'added') {
              const { id, fields, collection } = dataF;
              if (collection === AtShopCollections.shop_products) {
                ws.send(
                  `["{\\"msg\\":\\"sub\\",\\"id\\":\\"${makeid(
                    charsRS,
                    ids
                  )}\\",\\"name\\":\\"order.feedback\\",\\"params\\":[{\\"productId\\": \\"${id}\\" },{\\"skip\\":0,\\"limit\\":50}]}"]`
                );
                maxReadyCount++;
                products.push(dataF);
                await sleep(400);
              } else if (collection === AtShopCollections.shops) {
                shop = dataF;
                shop.store = store;
                console.log('Send full expired', shop.id);
                ws.send(
                  `"{\\"msg\\":\\"method\\",\\"id\\":\\"2\\",\\"method\\":\\"shop.isFullyExpired\\",\\"params\\":[\\"${shop.id}\\"]}"`
                );
                maxReadyCount++;
              } else if (collection === AtShopCollections.shop_order_feedback) {
                const f: AtShopFeedback = dataF;
                // Only push feedbacks with message
                if (f.msg) {
                  feedbacks.push(f);
                }
              } else if (collection === AtShopCollections.shop_product_groups) {
                groups.push(dataF);
                const productIds = dataF.fields.productIds
                  .map(function (el) {
                    return `\\"${el}\\"`;
                  })
                  .join(',');
                const idMade = makeid(charsRS, ids);
                const msgWs =
                  '["{\\"msg\\":\\"sub\\",\\"id\\":\\"' +
                  idMade +
                  '\\",\\"name\\":\\"shop.products.byId\\",\\"params\\":[\\"' +
                  idShop +
                  '\\",[' +
                  productIds +
                  ']]}"]';
                maxReadyCount++;
                ws.send(msgWs);
                await sleep(500);
              } else if (collection === AtShopCollections.shop_categories) {
                categories.push(dataF);
              } else if (collection === AtShopCollections.shop_gateways) {
                gateways.push(dataF);
              } else {
                console.log('Other collection', dataF);
              }
              if (id && fields && fields.domain) {
                console.log('Send webhooks 2');
                idShop = id;
                ws.send(
                  '["{\\"msg\\":\\"sub\\",\\"id\\":\\"x\\",\\"name\\":\\"shop.products\\",\\"params\\":[\\"' +
                    id +
                    '\\",250,false]}"]'
                );
                await sleep(500);
                ws.send(
                  '["{\\"msg\\":\\"sub\\",\\"id\\":\\"' +
                    makeid(charsRS, ids) +
                    '\\",\\"name\\":\\"shop.product.groups\\",\\"params\\":[\\"' +
                    id +
                    '\\"]}"]'
                );
                await sleep(500);
                ws.send(
                  '["{\\"msg\\":\\"sub\\",\\"id\\":\\"' +
                    makeid(charsRS, ids) +
                    '\\",\\"name\\":\\"shop.categories\\",\\"params\\":[\\"' +
                    id +
                    '\\"]}"]'
                );
                await sleep(500);
                ws.send(
                  '["{\\"msg\\":\\"sub\\",\\"id\\":\\"' +
                    makeid(charsRS, ids) +
                    '\\",\\"name\\":\\"shop.gateways\\",\\"params\\":[\\"' +
                    idShop +
                    '\\"]}"]'
                );
                await sleep(500);
              }
            } else if (dataF.msg === 'nosub') {
              readyCount++;
              console.log('Count', readyCount, maxReadyCount);
              if (maxReadyCount && readyCount >= maxReadyCount) {
                ws.close();
                if (timeout) clearTimeout(timeout);
                if (timeoutCancelMax) clearTimeout(timeoutCancelMax);
                return resolve({
                  shop,
                  products,
                  feedbacks,
                  groups,
                  categories,
                  gateways,
                });
              }
            } else {
              console.log('NOT RECOGNIZED', dataF);
              if (dataF.server_id === '0') {
                //console.log(dataF);
              }
            }
          }
          const readyCountBefore = readyCount + 0;
          if (timeout) clearTimeout(timeout);
          timeout = setTimeout(() => {
            if (readyCountBefore === readyCount && readyCount < maxReadyCount) {
              console.log(
                'Count still the same, 20 seconds without response',
                readyCountBefore,
                readyCount,
                maxReadyCount
              );
              ws.close();
              if (readyCount === 1 && maxReadyCount === 5) {
                return reject({ message: 'not_found' });
              }
              console.log('START AGAIN');
              return resolve(false);
            }
          }, 20000);
          try {
          } catch (e) {
            console.error(e);
            reject(e);
          }
        }
      });

      breakTimeout = setTimeout(() => {
        console.log('Check after 30 seconds if message received', msgReceived);
        if (!msgReceived) {
          ws.close();
          if (timeout) clearTimeout(timeout);
          if (timeoutCancelMax) clearTimeout(timeoutCancelMax);
          return resolve(false);
        }
      }, 30000);
    } catch (e) {
      console.error(e);
      reject(e);
    }
  }).catch((e) => {
    if (timeout) {
      clearTimeout(timeout);
      timeout = null;
    }
    if (breakTimeout) {
      clearTimeout(breakTimeout);
      breakTimeout = null;
    }
    return bError(e.message);
  });
  if (timeout) {
    clearTimeout(timeout);
    timeout = null;
  }
  if (breakTimeout) {
    clearTimeout(breakTimeout);
    breakTimeout = null;
  }
  if (result === false) {
    await sleep(5000);
    att++;
    result = await getAtShop(store, att);
  }
  return result;
};

export { getAtShop };
