import axios, { AxiosError, AxiosResponse } from 'axios';
import { load } from 'cheerio';
import fs from 'fs';

async function getIp() {
  const url = 'https://api.ipify.org/?format=json';
  const ip = await axios
    .get(url)
    .then((response) => response.data.ip as string);
  return ip;
}

async function req(url: string, data: any, cookie: string) {
  const formData = Object.keys(data)
    .map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    })
    .join('&');

  const resp = await axios
    .post(url, formData, {
      maxRedirects: 0,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(formData).toString(),
        Cookie: cookie,
      },
    })
    .then((response) => {
      console.log('Response:', response.statusText);
      return response;
    })
    .catch((error: AxiosError) => {
      if (error.response.status !== 302) {
        throw error;
      }
      return error.response;
    });
  return resp as AxiosResponse<any, any>;
}

async function login(username: string, password: string) {
  const url = 'https://admin.instantproxies.com/login_do.php';
  const data = {
    username,
    password,
    button: 'Sign In',
  };
  const resp = await req(url, data, '');
  const cookie = resp.headers['set-cookie']
    .map((el) => el.split(';')[0])
    .join(';');
  return { cookie, html: resp.data };
}

async function getIpHtml(html: string) {
  const $ = load(html);
  const ipLink = $('#addauthip-link').attr('href');
  const ipRegex = /addAuthIp\('(.*)'\)/;
  const match = ipLink.match(ipRegex);
  let ip = null;
  if (match && match[1]) {
    ip = match[1]; // This is the extracted IP address
  }
  return ip.trim();
}

async function replaceIp(ip: string, username: string, password: string) {
  const url = 'https://admin.instantproxies.com/main.php';
  let { cookie } = await login(username, password);
  console.log('cookie', cookie);

  const html = await axios
    .get(url, {
      headers: {
        Cookie: cookie,
      },
    })
    .then((res) => res.data);
  const $ = load(html);
  let ipsHtml = $('#authips-textarea').val() as string;
  console.log('ipsHtml', ipsHtml);
  if (!ipsHtml.includes(ip)) {
    try {
      const oldIp = fs.readFileSync('ip.txt', 'utf-8');
      ipsHtml = ipsHtml.replace(oldIp, ' ');
      // Convert all whitespaces into \r\n to make it easier to parse
      ipsHtml = ipsHtml.replace(/\s+/g, '\r\n').trim();
      console.log('Old ip removed');
    } catch (e) {
      console.error(e);
    }
    console.log('ipsHtml', ipsHtml);
    const txt = ip;
    const authips = txt + '\r\n' + ipsHtml;
    console.log('auth ips', authips);
    let data = {
      authips,
      cmd: 'Submit Update',
    };
    await req(url, data, cookie);
  }
}

async function main() {
  const timeout = setTimeout(() => {
    console.log('terminated');
    process.exit(1);
  }, 30000);
  const ip = await getIp();
  console.log('ip', ip);
  // 25 proxies update.
  await replaceIp(ip, '181046', 'g5nFKtX2GUb');
  await replaceIp(ip, '116858', 'VVWeyj58TxDD');

  console.log('finish, next in 60 seconds');
  // Save old ip
  fs.writeFileSync('ip.txt', ip);

  clearTimeout(timeout);
  // Convert the data object to x-www-form-urlencoded format
}

main();
