import dotenv from 'dotenv';
const cfg = dotenv.config();
const e = process.env;

const serverConfig = {
  port: 3212,
};

const recaptcha = {
  siteKey: '',
  secretKey: '',
  //siteKey: '6LcTNcMUAAAAALfLcgD0y-A5e6t4vefcFNdeH5ED',
  //secretKey: '6LcTNcMUAAAAADEpxYQ1v20gTFN4h4nHfC1kJqCs',
};

const mongoConfig = {
  database: 'axie_infinity',
};

const appUrl = '';
const maxInstances = 2;

export { serverConfig, recaptcha, mongoConfig, appUrl, maxInstances };
