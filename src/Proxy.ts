import fs from 'fs';
import { sleep } from './General/Utils';
export interface Proxy {
  host: string;
  port: number;
  protocol: string;
}

class ProxyService {
  private secondary_proxies = [
    '192.126.161.217:3128',
    '173.234.181.233:3128',
    '192.126.161.100:3128',
    '192.126.161.211:3128',
    '104.140.209.93:3128',
    '173.234.181.209:3128',
    '104.140.209.178:3128',
    '192.126.161.187:3128',
  ];
  private socks5_proxies = ['38.127.172.90:39080', '38.127.173.192:59182'];
  private proxies: Array<string> = [];
  private index = 0;
  private indexProxyUSA = 0;
  private indexProxyGB = 0;
  private proxyListUSA: Array<Proxy> = [
    ...[
      '192.126.161.217:8800',
      '173.234.181.233:8800',
      '192.126.161.100:8800',
      '192.126.161.211:8800',
      '104.140.209.93:8800',
      '173.234.181.209:8800',
      '104.140.209.178:8800',
      '192.126.161.187:8800',
    ].map(function (el) {
      const proxyUSA = el.split(':');
      return {
        host: proxyUSA[0],
        port: parseInt(proxyUSA[1]),
        protocol: 'http',
      };
    }),
  ];

  saveIndexProxyUSA(index: number) {
    try {
      fs.writeFileSync('indexProxyUSA.txt', index.toString());
    } catch (e) {}
  }

  setIndexProxyUSA() {
    try {
      this.indexProxyUSA = parseInt(
        fs.readFileSync('indexProxyUSA.txt', 'utf8'),
      );
      if (isNaN(this.indexProxyUSA)) this.indexProxyUSA = 0;
    } catch (e) {
      this.indexProxyUSA = 0;
    }
  }

  private proxyListGB: Array<Proxy> = [
    'proxyLess',
    '94.229.71.17:8800',
    '94.229.71.37:8800',
  ].map(function (el) {
    if (el == 'proxyLess') {
      return {
        host: el,
        port: 0,
        protocol: 'http',
      };
    } else {
      const proxy = el.split(':');
      return {
        host: proxy[0],
        port: parseInt(proxy[1]),
        protocol: 'http',
      };
    }
  });

  getNewProxy(): string {
    const newProxy = this.proxies[this.index];
    this.index++;
    if (this.index == this.proxies.length) this.index = 0;
    return newProxy;
  }

  constructor() {
    this.proxies = fs
      .readFileSync('proxies.txt', 'utf-8')
      .split('\n')
      .map((el) => 'http://' + el.trim());
    //this.setupPrivateProxies();
    this.setIndexProxyUSA();

    this.setIndex();
  }

  async startScrapProxies(timeout, country) {
    while (true) {
      await sleep(5 * 60 * 1000);
      //await this.setupProxyScrape('socks4', timeout, country);
      //await this.setupProxyScrape('socks5', timeout, country);
    }
  }

  // async setupProxyScrape(type, timeout, country) {
  //     const apiKey = 'PGYFN-26P7R-SCPX7-ODMEY';
  //     const url = `https://api.proxyscrape.com/?request=getproxies&proxytype=${type}&timeout=${timeout}&country=${country}&uptime=0&status=alive&averagetimeout=${timeout}&age=unlimited&score=0&port=all&organization=all&format=json&serialkey=${apiKey}`;
  //     const res: any = await rService.get(url);
  //     const proxies: Array<ProxyScrape> = res.data;
  //     this.proxies = proxies.map(({ proxy, protocol, port }) => {
  //         const host = proxy.split(':')[0];
  //         return {
  //             host: host,
  //             port: port,
  //             protocol: protocol,
  //         };
  //     });
  // }

  saveIndex() {
    try {
      fs.writeFileSync('index.txt', this.index.toString());
    } catch (e) {}
  }

  setIndex() {
    try {
      this.index = parseInt(fs.readFileSync('index.txt', 'utf8'));
      if (isNaN(this.index)) this.index = 0;
    } catch (e) {
      this.index = 0;
    }
  }

  getProxy(num: number) {
    let proxies = [];
    if (num === 1) proxies = this.proxies;
    else if (num === 2) proxies = this.secondary_proxies;
    else if (num === 3) proxies = this.socks5_proxies;
    if (this.index >= proxies.length) {
      this.index = 0;
    }
    this.saveIndex();
    const proxy = proxies[this.index];
    this.index++;
    return proxy;
  }

  getPrivateUSAProxy() {
    const inBounds = this.indexProxyUSA < this.proxyListUSA.length;
    if (!inBounds) this.indexProxyUSA = 0;
    const { host, port, protocol } = this.proxyListUSA[this.indexProxyUSA];
    this.saveIndexProxyUSA(this.indexProxyUSA);
    this.indexProxyUSA++;
    return protocol + '://' + host + ':' + port;
  }

  getPrivateGBProxy() {
    const inBounds = this.indexProxyGB < this.proxyListGB.length;
    if (!inBounds) this.indexProxyGB = 0;
    const { host, port, protocol } = this.proxyListGB[this.indexProxyGB];
    this.indexProxyGB++;
    return {
      host: host,
      port: port,
      protocol: protocol,
    };
  }

  private proxyListFP: Array<Proxy> = [
    '94.229.71.37:8800',
    '192.126.161.217:8800',
    '173.234.181.233:8800',
    '192.126.161.100:8800',
    '192.126.161.211:8800',
    '104.140.209.93:8800',
    '94.229.71.17:8800',
    '173.234.181.209:8800',
    '104.140.209.178:8800',
    '192.126.161.187:8800',
  ].map(function (el) {
    if (el == 'proxyLess') {
      return {
        host: el,
        port: 0,
        protocol: 'http',
      };
    } else {
      const proxy = el.split(':');
      return {
        host: proxy[0],
        port: parseInt(proxy[1]),
        protocol: 'http',
      };
    }
  });

  private indexProxyFP = 0;
  getPrivateFootPatrolProxy() {
    const inBounds = this.indexProxyFP < this.proxyListFP.length;
    if (!inBounds) this.indexProxyFP = 0;
    const { host, port, protocol } = this.proxyListFP[this.indexProxyFP];
    this.indexProxyFP++;
    return {
      host: host,
      port: port,
      protocol: protocol,
    };
  }

  getConfigNewProxy(): string {
    return this.getPrivateUSAProxy();
  }
}

const proxyObj = new ProxyService();
export default proxyObj;
