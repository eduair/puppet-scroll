// Add humanize plugin
import axios from 'axios';
import dotenv from 'dotenv';
import fs from 'fs';
import { createCursor } from 'ghost-cursor';
import moment from 'moment';
import os from 'os';
import { Browser, BrowserContext, ConnectOptions, Page } from 'puppeteer';
import puppeteer from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import UserAgent from 'user-agents';
import { sleep } from '../General/Utils';
import { buildError, buildSuccess } from '../global';
puppeteer.use(StealthPlugin());
const cfg = dotenv.config();

function findString(body, begin, end) {
  if (!body) {
    return false;
  }
  body = body.split(begin);
  body = body[1];
  if (body) {
    body = body.split(end);
    body = body[0];
    return body;
  } else {
    return false;
  }
}

export class Puppet {
  private initialPageError = false;
  public browser: Browser;
  private incognito: BrowserContext;
  private ws: string;
  page: Page;

  getChromePath() {
    const osPlatform = os.platform(); // possible values are: 'darwin', 'freebsd', 'linux', 'sunos' or 'win32'
    let executablePath: string;
    if (/^win/i.test(osPlatform)) {
      executablePath =
        'C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe';
    } else if (/^linux/i.test(osPlatform)) {
      executablePath = '/usr/bin/google-chrome';
    }
    return executablePath;
  }

  async blockRequests(page: Page) {
    await page.setRequestInterception(true);
    page.on('request', (req) => {
      // List of resource types to block
      const blockedResourceTypes = ['image', 'stylesheet', 'font'];

      // If the resource type is in the blocked list, abort the request
      if (blockedResourceTypes.includes(req.resourceType())) {
        req.abort();
      } else {
        req.continue();
      }
    });
  }

  async checkResponsesAuth(page: any) {
    return new Promise((resolve) => {
      const keyword = 'authorize/accept';
      setTimeout(() => {
        return resolve(false);
      }, 60000);
      page.on('response', async (response) => {
        if (response) {
          const url = response.url();
          const check = url.includes(keyword);
          if (check) {
            const headers = response._headers.location;
            const token = decodeURIComponent(
              findString(headers, 'access_token=', '&'),
            );
            console.log('TOKEN::', token);
            return resolve(token);
          }
        }
      });
    });
  }

  private cursor;
  async createCursor() {
    if (this.cursor) return this.cursor;
    this.cursor = createCursor(this.page);
    return this.cursor;
  }

  async startBrowserSellix(proxy: string) {
    try {
      var args = ['--app=https://sellix.io', '--no-sandbox'];
      if (proxy && proxy !== 'proxyless') {
        console.log('Proxy', proxy);
        var proxyArg = `--proxy-server=${proxy}`;
        args.push(proxyArg);
      }
      const headless = process.env.headless === 'false' ? false : true;
      const opts: any = {
        headless: headless,
        args: args,
        executablePath: this.getChromePath(),
      };
      this.browser = (await puppeteer.launch(opts).catch((e) => {
        console.error('31', e);
        return { error: e.message };
      })) as any;

      if (this.browser && (this.browser as any).error) {
        return this.error((this.browser as any).error, 'Browser start failed');
      } else {
        this.browser.on('disconnected', () => {
          this.browser = null;
        });
      }
    } catch (e) {
      return this.error(e.message, 'Browser start failed');
    }
  }

  saveWs(ws: string) {
    // http://127.0.0.1:9222/json/version
    try {
      return fs.writeFileSync('puppetWs.txt', ws);
    } catch (e) {
      console.error(e);
    }
    return '';
  }

  getWs() {
    // http://127.0.0.1:9222/json/version
    try {
      return fs.readFileSync('puppetWs.txt', 'utf8');
    } catch (e) {
      console.error(e);
    }
    return '';
  }

  async startBrowser(proxy: string) {
    try {
      const wsPort = process.env.WS_PORT || 9222;
      const options = {
        width: process.env.width ? parseInt(process.env.width) : 500,
        height: process.env.height ? parseInt(process.env.height) : 600,
      };
      const optionPosition = {
        x: process.env.x ? parseInt(process.env.x) : 900,
        y: process.env.y ? parseInt(process.env.y) : 0,
      };
      var args = [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--remote-debugging-port=' + wsPort,
        `--window-size=${options.width},${options.height}`,
        `--window-position=${optionPosition.x},${optionPosition.y}`,
      ];
      const isProxyless = process.env.proxyless === 'true';
      const useProxy = proxy && proxy !== 'proxyless' && !isProxyless;
      console.log('use proxy', useProxy);
      if (useProxy) {
        console.log('Proxy', proxy);
        var proxyArg = `--proxy-server=${proxy}`;
        args.push(proxyArg);
      }
      const headless = process.env.headless === 'false' ? false : true;
      const opts: any = {
        headless: headless,
        args: args,
        executablePath: this.getChromePath(),
      };
      const ws = this.getWs();
      this.ws = ws;
      if (ws) {
        console.log('Connect WS');
        let jsonConnect: ConnectOptions = {
          browserWSEndpoint: ws,
        };
        console.log('Connection WS', jsonConnect);
        try {
          this.browser = await puppeteer.connect(jsonConnect);
          console.log('Finish connect WS');
        } catch (e) {
          console.log('Connection to WS failed, starting new browser');
          this.saveWs('');
          return this.startBrowser(proxy);
        }
      } else {
        console.log('Launch Puppeteer', args);
        this.browser = (await puppeteer.launch(opts).catch((e) => {
          console.error('31', e);
          return { error: e.message };
        })) as any;

        const targets = this.browser.targets();
        const target = targets.find((target) => target.type() === 'browser');
        if (target) {
          let att = 0;
          while (att < 5) {
            const data = await axios
              .get('http://127.0.0.1:' + wsPort + '/json/version')
              .then((res) => res.data)
              .catch((e) => null);
            if (data) {
              console.log('Save new ws');
              this.saveWs(data.webSocketDebuggerUrl);
              this.ws = data.webSocketDebuggerUrl;
              break;
            }
            att++;
            await sleep(150);
          }
        }
      }

      if (this.browser && (this.browser as any).error) {
        return this.error((this.browser as any).error, 'Browser start failed');
      } else {
        this.browser.on('disconnected', () => {
          this.browser = null;
        });
      }
    } catch (e) {
      return this.error(e.message, 'Browser start failed');
    }
  }

  async authenticate(username, password) {
    await this.page.authenticate({ username: username, password: password });
  }

  async getPageScrollingStatus(page) {
    const pageStatus = await page.evaluate(
      () => document.scrollingElement.scrollTop,
    );
    return pageStatus;
  }

  async scrollToBottom(page, timeout, distance = 1000) {
    let previousStatus;
    const maxStops = 3;
    const delay = 500;
    let scrollStops = 0;
    let start = moment();
    // eslint-disable-next-line no-constant-condition
    while (true) {
      console.log('Scroll');
      const pageStatus = await this.getPageScrollingStatus(page);
      if (previousStatus !== undefined && previousStatus === pageStatus) {
        scrollStops++;
        console.log('Scroll Stops', scrollStops, previousStatus, pageStatus);
        await sleep(600);
        if (scrollStops >= maxStops) {
          console.log('Break');
          break;
        }
      } else {
        previousStatus = pageStatus;
        scrollStops = 0;
      }
      await page.evaluate((y) => {
        document.scrollingElement.scrollBy(0, y);
      }, distance);
      await sleep(delay);
      let timeNow = moment();
      const diff = timeNow.diff(start);
      console.log('Diff', diff);
      if (diff > timeout) break;
    }
    const res = await page.evaluate(async () => {
      return await new Promise((resolve) => {
        return resolve(document.body.innerHTML);
      });
    });
    return res;
  }

  async setBypassMethods() {
    await this.page.setRequestInterception(true);
    // Pass the User-Agent Test.
    const userAgent = new UserAgent();
    await this.page.setUserAgent(userAgent.toString());

    // Pass the Webdriver Test.
    await this.page.evaluateOnNewDocument(() => {
      Object.defineProperty(navigator, 'webdriver', {
        get: () => false,
      });
    });

    // Pass the Chrome Test.
    await this.page.evaluateOnNewDocument(() => {
      // We can mock this in as much depth as we need for the test.
      const n: any = window.navigator;
      n.chrome = {
        runtime: {},
        // etc.
      };
    });

    // Pass the Permissions Test.
    await this.page.evaluateOnNewDocument(() => {
      const originalQuery = window.navigator.permissions.query;
      const p: any = window.navigator.permissions;
      return (p.query = (parameters: any) =>
        parameters.name === 'notifications'
          ? Promise.resolve({ state: Notification.permission })
          : originalQuery(parameters));
    });

    // Pass the Languages Test.
    await this.page.evaluateOnNewDocument(() => {
      // Overwrite the `plugins` property to use a custom getter.
      Object.defineProperty(navigator, 'languages', {
        get: () => ['es-ES', 'es'],
      });
    });

    // Pass the Plugins Length Test.
    await this.page.evaluateOnNewDocument(() => {
      // Overwrite the `plugins` property to use a custom getter.
      Object.defineProperty(navigator, 'plugins', {
        // This just needs to have `length > 0` for the current test,
        // but we could mock the plugins too if necessary.
        get: () => [1, 2, 3, 4, 5],
      });
    });

    this.page.on('request', (request) => {
      if (
        [
          'image',
          'stylesheet',
          'font',
          'vnd.microsoft.icon',
          'svg+xml',
          'png',
          'text/javascript',
        ].indexOf(request.resourceType()) !== -1
      ) {
        request.abort();
      } else {
        request.continue();
      }
    });

    await this.page.setViewport({
      width: Math.floor(Math.random() * 1920) + 1080,
      height: Math.floor(Math.random() * 1080) + 800,
    });
  }

  async goTo(url: string, waitBody: boolean, waitFor?: string) {
    try {
      const resPage: any = await this.page.goto(url).catch((e) => {
        return { error: e.message };
      });
      if (resPage.error) {
        return this.error(resPage.error, url);
      }
      if (waitFor) {
        await this.page.waitForSelector(waitFor, { timeout: 1000000 });
      } else {
        if (waitBody) {
          await this.page.$('body').catch(() => {});
        }
      }
    } catch (e) {
      return this.error(e.message, url);
    }
  }

  async waitBody() {
    await this.page.$('body').catch(() => {});
  }

  async humanClick(btnRef: string, notClose?: boolean, waitFor?: string) {
    try {
      await this.page.click(btnRef);
    } catch (e) {
      return this.error(e.message, btnRef, notClose);
    }
  }

  async autoScroll(page, waitFor = null) {
    console.log('Wait For', waitFor);
    const res = await page.evaluate(async (waitFor) => {
      console.log('Evaluate', waitFor);
      return await new Promise((resolve, reject) => {
        var totalHeight = 0;
        var distance = 100;
        var timer = setInterval(() => {
          var scrollHeight = document.body.scrollHeight;
          window.scrollBy(0, distance);
          totalHeight += distance;

          if (totalHeight >= scrollHeight && !waitFor) {
            clearInterval(timer);
            return resolve(document.body.innerHTML);
          } else {
            const foundSelector = document.querySelector(waitFor);
            if (foundSelector) {
              return resolve(document.body.innerHTML);
            }
          }
        }, 100);
      });
    }, waitFor);
    return res;
  }

  async clickBtn(btnRef: string, notClose?: boolean, waitFor?: string) {
    try {
      await this.page.waitForSelector(btnRef);
      await this.page.$eval(btnRef, (elem) => (elem as any).click(), btnRef);
      if (waitFor) {
        await this.page.waitForSelector(waitFor);
      }
    } catch (e) {
      return this.error(e.message, btnRef, notClose);
    }
  }

  async clickBtnPopup(
    btnRef: string,
    notClose?: boolean,
    waitFor?: string,
    popup: any = null,
  ) {
    let page = this.page;
    if (popup) {
      page = popup;
    }
    await page.waitForSelector(btnRef);
    await page.$eval(btnRef, (elem) => (elem as any).click(), btnRef);
    if (waitFor) {
      await page.waitForSelector(waitFor);
    }
  }

  async openPopup(clickBtn: string, selector: string) {
    const page = this.page;
    await page.click(clickBtn);
    const newPagePromise = new Promise((x) =>
      this.browser.once('targetcreated', (target) => x(target.page())),
    );
    const popup: any = await newPagePromise;
    //await popup.waitForSelector(selector);
    return popup;
  }

  async typeOnInputPopup(input: string, toType: string, popup: any = null) {
    if (popup) {
      let elInput = (await popup.$$(input))[0];
      if (elInput) {
        await elInput.type(toType);
      } else {
        await popup.waitForSelector(input);
        elInput = (await popup.$$(input))[0];
        await elInput.type(toType);
      }
    } else {
      const elInput = (await this.page.$$(input))[0];
      await elInput.type(toType);
    }
  }

  async checkResponsesPopup(page: any) {
    return new Promise((resolve) => {
      const keyword = 'login/password';
      setTimeout(() => {
        return resolve(false);
      }, 7000);
      page.on('response', async (response) => {
        if (response) {
          const url = response.url();
          const check = url.includes(keyword);
          if (check) {
            const text = await response.json();
            return resolve({ status: response.status(), text });
          }
        }
      });
    });
  }

  async getCookiesPopup(page: any) {
    const cookies = await await page.cookies();
    return cookies;
  }

  async getCookies() {
    const cookies = await await this.page.cookies();
    return cookies;
  }

  async checkResponses() {
    return new Promise((resolve) => {
      const keyword = 'login/password';
      setTimeout(() => {
        return resolve(false);
      }, 10000);
      this.page.on('response', async (response) => {
        if (response) {
          const url = response.url();
          const check = url.includes(keyword);
          if (check) {
            try {
              const text = await response.json();
              return resolve({ status: response.status(), text });
            } catch (e) {
              return resolve({ status: response.status(), text: '' });
            }
          }
        }
      });
    });
  }

  async readDiv(divSearch, notClose?: boolean) {
    try {
      let element = await this.page.$(divSearch);
      let value = await this.page.evaluate((el) => el.textContent, element);
      if (value) {
        return buildSuccess(value);
      } else {
        return this.error('No DIV', divSearch, notClose);
      }
    } catch (e) {
      return this.error('NO DIV ' + e.message, divSearch, notClose);
    }
  }

  async checkSpecificError(errorDiv, text: string, notClose?: boolean) {
    try {
      await this.page.waitForSelector(errorDiv);
      let element = await this.page.$(errorDiv);
      let value = await this.page.evaluate((el) => el.textContent, element);
      console.log(value);
      if (value && (value == text || value.includes(text))) {
        return this.error(value, '', notClose);
      } else {
        return buildSuccess('No error');
      }
    } catch (e) {
      return buildSuccess('No error');
    }
  }

  async checkError(errorDiv, notClose?: boolean) {
    try {
      await this.page.waitForSelector(errorDiv);
      let element = await this.page.$(errorDiv);
      let value = await this.page.evaluate((el) => el.textContent, element);
      console.log(value);
      if (value) {
        return this.error(value, '', notClose);
      } else {
        //return buildSuccess("No error");
      }
    } catch (e) {
      //return buildSuccess("No error");
    }
  }

  async typeOnInput(input: string, toType: string) {
    try {
      const elInput = (await this.page.$$(input))[0];
      await elInput.type(toType);
    } catch (e) {
      return this.error(e.message, input);
    }
  }

  async createIncognito() {
    const incognito = await (
      this.browser as Browser
    ).createIncognitoBrowserContext();
    this.incognito = incognito;
  }

  async createPageIncognito() {
    this.page = await this.incognito.newPage();
  }

  async disconnectBrowser() {
    this.browser.disconnect();
    this.browser = null;
  }

  async createPage(useAlreadyCreated = false) {
    if (useAlreadyCreated) {
      const pages = await this.browser.pages();
      if (pages.length) {
        this.page = pages[0];
        return;
      }
    }
    this.page = await this.browser.newPage();
  }

  private async error(string, extra, noClose?: boolean) {
    console.error('ERROR', string, extra);
    await this.closeBrowser().catch((e) => console.log(e));
    return buildError(string + extra);
  }

  async closeBrowser() {
    if (!this.browser) {
      console.log('NO BROWSER');
      return;
    }
    if (this.browser) {
      try {
        let pages: Page[] = await this.browser.pages().catch((e) => {
          return [];
        });
        for (let page of pages) {
          page.removeAllListeners();
        }
        const res = await this.browser.close().catch((e) => {
          return { error: e.message };
        });
      } catch (e) {
        console.error('ERROR CLOSING BROWSER::', e);
        if (this.browser) {
          try {
            await this.browser.close();
          } catch (e) {
            // Exit process, there is no browser.
            process.exit(1);
          }
        }
      } finally {
        if (this.browser) {
          try {
            await this.browser.close();
          } catch (e) {
            // Exit process, there is no browser.
            process.exit(1);
          }
        }
      }
    }
    return;
  }

  async puppetTest(page, proxy = null) {}
}

export var puppet = new Puppet();
