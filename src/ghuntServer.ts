process.setMaxListeners(0);
import { recaptcha } from './config/config';
import Express from './Express/Express';
import { GHuntReq, GHuntReqPing } from './Ghunt';

const server = new Express(8721, '/', recaptcha);
class Main {
  constructor() {
    console.log('Ghunt Server');
    this.init();
  }
  init() {
    server.postJsonAlt2('ghunt', GHuntReq);
    server.postJsonAlt2('ghunt/ping', GHuntReqPing);
  }
}

async function start() {
  new Main();
}

start();
