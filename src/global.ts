export function buildSuccess(string) {
  return { success: string, error: false };
}

export function buildError(string) {
  return { error: string, success: false };
}
