import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import dotenv from 'dotenv';
import { Request, Response } from 'express';
import { GeminiResponse } from './gemini.interface';
dotenv.config();

interface InlineData {
  mimeType: string;
  data: string;
}

class Gemini {
  apiKey = process.env.API_KEY;

  req = async (req: Request, res: Response): Promise<any> => {
    const prompt = req.body.prompt;
    const inlineData = req.body.inlineData;
    const history = req.body.history;
    const instructions = req.body.instructions;
    const text = await this.generateContentReq(
      prompt,
      inlineData,
      history,
      instructions,
    );
    return { text };
  };

  async generateContentReq(
    prompt: string,
    inlineData?: InlineData,
    history?: any[],
    instructions?: string,
  ) {
    //const model = inlineData ? 'gemini-1.5-pro' : 'gemini-pro';
    const model = 'gemini-1.5-flash-exp-0827';
    const url =
      `https://generativelanguage.googleapis.com/v1beta/models/${model}:generateContent?key=` +
      this.apiKey;
    const opt: AxiosRequestConfig = {
      headers: { 'Content-Type': 'application/json' },
    };
    let parts: any[] = [{ text: prompt }];
    if (inlineData) {
      parts.push({
        inlineData: {
          mime_type: inlineData.mimeType,
          data: inlineData.data,
        },
      });
    }
    let contents = [];
    if (history && history.length) {
      contents = history.map((el) => {
        if (el.role === 'client') {
          el.role = 'model';
        }
        return el;
      });
    }
    contents.push([
      {
        parts,
        role: 'user',
      },
    ]);
    let system_instruction: any = undefined;
    if (instructions) {
      system_instruction = {
        parts: { text: instructions },
      };
    }

    const res = (await axios
      .post(
        url,
        {
          system_instruction,
          contents,
        },
        opt,
      )
      .then((res) => res.data)
      .catch((e: AxiosError) => {
        console.log(e.message);
        if (e.response.data) {
          console.log('Data response', e.response.data);
        }
        return '';
      })) as GeminiResponse;

    if (res) {
      return res.candidates[0].content.parts.map((el) => el.text).join(' ');
    }

    return '';
  }
}

const gemini = new Gemini();
export { gemini };
