"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
process.setMaxListeners(0);
const ExpressSetup_1 = __importDefault(require("./Express/ExpressSetup"));
const Puppet_1 = __importDefault(require("./Puppet"));
class Main {
    constructor() {
        console.log('Test');
        this.init();
    }
    async init() {
        await Puppet_1.default.startBrowser();
        ExpressSetup_1.default.postJson('sell_app_single', Puppet_1.default.getSingleProductReq);
        ExpressSetup_1.default.postJson('', Puppet_1.default.getPageRequest);
        ExpressSetup_1.default.postJson('sell_pass', Puppet_1.default.getSellPass);
    }
}
async function start() {
    new Main();
}
start();
//# sourceMappingURL=index.js.map