"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const util_1 = __importDefault(require("util"));
const exec = util_1.default.promisify(require('child_process').exec);
const app = (0, express_1.default)();
app.use((0, cors_1.default)());
app.set('json spaces', 0);
app.use(express_1.default.json());
app.set('trust proxy', true);
app.use(express_1.default.urlencoded({ extended: false }));
app.get('/screenshot', async (req, res) => {
    const { stdout, stderr } = await exec('python computer/screenshot.py', {
        windowsHide: true,
    });
    console.log('Response', stdout);
    const pathFile = path_1.default.join(__dirname, '..', 'screenshot.png');
    console.log('pathFile', pathFile);
    res.sendFile(pathFile, (err) => {
        if (err) {
            console.error(`Error serving image: ${err.message}`);
            res.status(404).send('Image not found');
        }
    });
});
const port = 2345;
app.listen(port, () => {
    console.log(`Screenshot App listening at http://localhost:${port}`);
});
//# sourceMappingURL=image_server.js.map