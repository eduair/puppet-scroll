"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const getIP = require('external-ip')();
const axios_1 = __importDefault(require("axios"));
async function getIp() {
    const url = 'https://api.ipify.org/?format=json';
    const ip = await axios_1.default
        .get(url)
        .then((response) => response.data.ip);
    return ip;
}
const setIp = async () => {
    const ip = await getIp();
    console.log(ip);
    const res = await axios_1.default
        .post('http://104.234.204.107:3500/dead_air/set_ip', { ip })
        .then((res) => res.data);
    const res2 = await axios_1.default
        .post('http://83.147.54.179:8082/set_ip', { ip })
        .then((res) => res.data)
        .catch((e) => console.error(e));
    console.log('ip response', res, res2);
    setTimeout(() => {
        setIp();
    }, 5000);
};
setIp();
//# sourceMappingURL=set_ip.js.map