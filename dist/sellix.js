"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
process.setMaxListeners(0);
const config_1 = require("./config/config");
const Express_1 = __importDefault(require("./Express/Express"));
const Gemini_1 = require("./Gemini/Gemini");
const Ghunt_1 = require("./Ghunt");
const Puppet_1 = __importDefault(require("./Puppet"));
const server = new Express_1.default(8720, '/', config_1.recaptcha);
class Main {
    constructor() {
        console.log('Sellix Server');
        this.init();
    }
    init() {
        server.postJsonAlt('sellix', Puppet_1.default.getSellix);
        server.postJsonAlt('atshop', Puppet_1.default.getAtshop);
        server.postJson('gemini', Gemini_1.gemini.req);
        server.postJsonAlt('ghunt', Ghunt_1.GHuntReq);
        server.postJsonAlt('ghunt/ping', Ghunt_1.GHuntReqPing);
    }
}
async function start() {
    new Main();
}
start();
//# sourceMappingURL=sellix.js.map