"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Puppet_1 = __importDefault(require("./Puppet"));
const SellApp_1 = require("./SellApp");
async function main() {
    const timer = setTimeout(() => {
        console.log('Timeout, restart process');
        process.exit(1);
    }, 60000);
    // const entry = {
    //   user: 'trusthub',
    //   type: 'sell_app',
    // } as EntryI;
    const { entry, url } = await SellApp_1.sellApp.getStore();
    //const url = 'https://trusthub.sell.app';
    const response = await Puppet_1.default.getSellAppPage(url, false, '', timer);
    if (response.status !== 500) {
        console.log('Valid page', url);
        response.username = entry.user;
        console.log('To upload', response);
        const uploadResponse = await SellApp_1.sellApp.uploadStore(entry, response);
        console.log(uploadResponse);
    }
    else {
        console.log('Error getting page info', response);
        process.exit(1);
    }
}
main();
//# sourceMappingURL=test_sellapp.js.map