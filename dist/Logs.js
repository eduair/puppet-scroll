"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chalk_1 = __importDefault(require("chalk"));
const logs = {
    info: (toLog) => {
        console.log(chalk_1.default.blue(toLog));
    },
    success: (toLog) => {
        console.log(chalk_1.default.green(toLog));
    },
    error: (toLog) => {
        console.log(chalk_1.default.red(toLog));
    },
};
exports.default = logs;
//# sourceMappingURL=Logs.js.map