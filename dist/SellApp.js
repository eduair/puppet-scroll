"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sellApp = void 0;
const axios_1 = __importDefault(require("axios"));
const axios_retry_1 = __importDefault(require("axios-retry"));
const Utils_1 = require("./General/Utils");
class SellApp {
    constructor() {
        this.axios = axios_1.default.create({
            baseURL: 'http://104.234.204.107:3499/',
            headers: {
                dev: 'shellix',
            },
        });
        (0, axios_retry_1.default)(this.axios, { retries: 3 });
    }
    buildLink(data) {
        return `https://${data.user}.sell.app`;
    }
    async getStore() {
        try {
            const data = await this.axios
                .get('sell_app/last')
                .then((res) => res.data);
            return { entry: data, url: this.buildLink(data) };
        }
        catch (e) {
            await (0, Utils_1.sleep)(1000);
            return this.getStore();
        }
    }
    async uploadStore(entry, shop) {
        try {
            const data = await this.axios
                .post('sell_app/update', { entry, shop })
                .then((res) => res.data);
            return data;
        }
        catch (e) {
            await (0, Utils_1.sleep)(1000);
            return this.uploadStore(entry, shop);
        }
    }
}
exports.sellApp = new SellApp();
//# sourceMappingURL=SellApp.js.map