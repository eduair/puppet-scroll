"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const cheerio_1 = require("cheerio");
const fs_1 = __importDefault(require("fs"));
async function getIp() {
    const url = 'https://api.ipify.org/?format=json';
    const ip = await axios_1.default
        .get(url)
        .then((response) => response.data.ip);
    return ip;
}
async function req(url, data, cookie) {
    const formData = Object.keys(data)
        .map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    })
        .join('&');
    const resp = await axios_1.default
        .post(url, formData, {
        maxRedirects: 0,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(formData).toString(),
            Cookie: cookie,
        },
    })
        .then((response) => {
        console.log('Response:', response.statusText);
        return response;
    })
        .catch((error) => {
        if (error.response.status !== 302) {
            throw error;
        }
        return error.response;
    });
    return resp;
}
async function login(username, password) {
    const url = 'https://admin.instantproxies.com/login_do.php';
    const data = {
        username,
        password,
        button: 'Sign In',
    };
    const resp = await req(url, data, '');
    const cookie = resp.headers['set-cookie']
        .map((el) => el.split(';')[0])
        .join(';');
    return { cookie, html: resp.data };
}
async function getIpHtml(html) {
    const $ = (0, cheerio_1.load)(html);
    const ipLink = $('#addauthip-link').attr('href');
    const ipRegex = /addAuthIp\('(.*)'\)/;
    const match = ipLink.match(ipRegex);
    let ip = null;
    if (match && match[1]) {
        ip = match[1]; // This is the extracted IP address
    }
    return ip.trim();
}
async function replaceIp(ip, username, password) {
    const url = 'https://admin.instantproxies.com/main.php';
    let { cookie } = await login(username, password);
    console.log('cookie', cookie);
    const html = await axios_1.default
        .get(url, {
        headers: {
            Cookie: cookie,
        },
    })
        .then((res) => res.data);
    const $ = (0, cheerio_1.load)(html);
    let ipsHtml = $('#authips-textarea').val();
    console.log('ipsHtml', ipsHtml);
    if (!ipsHtml.includes(ip)) {
        try {
            const oldIp = fs_1.default.readFileSync('ip.txt', 'utf-8');
            ipsHtml = ipsHtml.replace(oldIp, ' ');
            // Convert all whitespaces into \r\n to make it easier to parse
            ipsHtml = ipsHtml.replace(/\s+/g, '\r\n').trim();
            console.log('Old ip removed');
        }
        catch (e) {
            console.error(e);
        }
        console.log('ipsHtml', ipsHtml);
        const txt = ip;
        const authips = txt + '\r\n' + ipsHtml;
        console.log('auth ips', authips);
        let data = {
            authips,
            cmd: 'Submit Update',
        };
        await req(url, data, cookie);
    }
}
async function main() {
    const timeout = setTimeout(() => {
        console.log('terminated');
        process.exit(1);
    }, 30000);
    const ip = await getIp();
    console.log('ip', ip);
    // 25 proxies update.
    await replaceIp(ip, '181046', 'g5nFKtX2GUb');
    await replaceIp(ip, '116858', 'VVWeyj58TxDD');
    console.log('finish, next in 60 seconds');
    // Save old ip
    fs_1.default.writeFileSync('ip.txt', ip);
    clearTimeout(timeout);
    // Convert the data object to x-www-form-urlencoded format
}
main();
//# sourceMappingURL=update_ip.js.map