"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MiddleWare {
    async verifyRequest(req, res, next) {
        next();
    }
}
const middleware = new MiddleWare();
exports.default = middleware;
//# sourceMappingURL=Middleware.js.map