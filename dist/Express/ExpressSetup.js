"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config/config");
const Express_1 = __importDefault(require("./Express"));
const server = new Express_1.default(config_1.serverConfig.port, '/', config_1.recaptcha);
exports.default = server;
//# sourceMappingURL=ExpressSetup.js.map