"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
// @ts-ignore
const express_queue_1 = __importDefault(require("express-queue"));
const express_recaptcha_1 = require("express-recaptcha");
const express_validator_1 = require("express-validator");
const http = __importStar(require("http"));
const Utils_1 = require("../General/Utils");
const Logs_1 = __importDefault(require("../Logs"));
const config_1 = require("../config/config");
const Middleware_1 = __importDefault(require("./Middleware"));
class Express {
    constructor(port, baseUrl, recaptchaKey = {
        siteKey: '',
        secretKey: '',
    }) {
        this.app = (0, express_1.default)();
        this.baseUrl = baseUrl;
        this.port = port;
        if (recaptchaKey.siteKey) {
            this.recaptcha = new express_recaptcha_1.RecaptchaV2(recaptchaKey.siteKey, recaptchaKey.secretKey);
        }
        //const origin = 'https://shellix.xyz';
        const origin = process.env.origin;
        //const options = { credentials: true, origin: [origin, 'http://localhost:3111'] };
        this.app.use((0, cors_1.default)());
        this.app.use(express_1.default.json({ limit: '50mb' }));
        this.app.set('trust proxy', true);
        this.app.use(express_1.default.urlencoded({ extended: false }));
        this.start();
        this.app.use(Middleware_1.default.verifyRequest);
    }
    useCache() {
        // const cacheMiddleware = new ExpressCache(
        //   cacheManager.caching({
        //     store: 'memory',
        //     max: 100000,
        //     ttl: 240,
        //   }),
        //   {
        //     hydrate: (req, res, data, cb) => {
        //       // Parse as JSON first
        //       try {
        //         const j = JSON.parse(data)
        //         res.contentType('application/json')
        //         return res.json(j)
        //       } catch (err) {
        //         console.error(err)
        //       }
        //       const f: any = fileType
        //       const guess = f(data.slice(0, 4101))
        //       if (guess) {
        //         res.contentType(guess.mime)
        //       }
        //       return data
        //     },
        //     getCacheKey: (req: Request) => {
        //       const j = req.body ? JSON.stringify(req.body) : ''
        //       const q = req.query ? JSON.stringify(req.query) : ''
        //       const url = req.url
        //       const data = `${j}${q}${url}`
        //       return crypto.createHash('md5').update(data).digest('hex')
        //     },
        //   }
        // )
        // cacheMiddleware.attach(this.app)
    }
    getApp() {
        return this.app;
    }
    getRecaptchaMiddleWare() {
        if (this.recaptcha)
            return this.recaptcha.middleware.verify;
    }
    getJson(requestUrl, f) {
        this.app.get(`${this.baseUrl}${requestUrl}`, async (req, res) => {
            const result = await f(req, res).catch((e) => {
                return (0, Utils_1.bError)(e.message);
            });
            res.json(result);
        });
    }
    get(requestUrl, f) {
        this.app.get(`${this.baseUrl}${requestUrl}`, async (req, res) => {
            f(req, res).catch(() => {
                res.status(404);
                res.end();
            });
        });
    }
    confirmPull(requestUrl, f) {
        this.app.post(`${this.baseUrl}${requestUrl}`, async (req, res) => {
            res.json({ received: true });
            const result = await f(req);
        });
    }
    postJsonAlt2(requestUrl, f, validation = []) {
        this.app.post(`${this.baseUrl}${requestUrl}`, validation, async (req, res) => {
            const errors = (0, express_validator_1.validationResult)(req);
            if (!errors.isEmpty())
                return res.json({ error: errors.array() });
            return f(req, res);
        });
    }
    postJsonAlt(requestUrl, f, validation = []) {
        this.app.post(`${this.baseUrl}${requestUrl}`, (0, express_queue_1.default)({ activeLimit: config_1.maxInstances, queuedLimit: 10 }), validation, async (req, res) => {
            const errors = (0, express_validator_1.validationResult)(req);
            if (!errors.isEmpty())
                return res.json({ error: errors.array() });
            return f(req, res);
        });
    }
    postJson(requestUrl, f, validation = []) {
        this.app.post(`${this.baseUrl}${requestUrl}`, (0, express_queue_1.default)({ activeLimit: config_1.maxInstances, queuedLimit: 10 }), validation, async (req, res) => {
            const errors = (0, express_validator_1.validationResult)(req);
            if (!errors.isEmpty())
                return res.json({ error: errors.array() });
            const result = await f(req, res).catch((e) => {
                console.error(e);
                return (0, Utils_1.bError)(e.message);
            });
            res.json(result);
        });
    }
    postJsonRecaptcha(requestUrl, f, validation = []) {
        this.app.post(`${this.baseUrl}${requestUrl}`, this.recaptcha.middleware.verify, async (req, res) => {
            if (req['recaptcha'] && !req['recaptcha']['error']) {
                const errors = (0, express_validator_1.validationResult)(req);
                if (!errors.isEmpty())
                    return { error: errors.array() };
                const result = await f(req).catch((e) => {
                    return (0, Utils_1.bError)(e.message);
                });
                res.json(result);
            }
            else {
                res.json({ error: 'Bad recaptcha' });
            }
        });
    }
    async start() {
        const server = http.createServer(this.app);
        server.listen(this.port, () => {
            Logs_1.default.success('Express server listening on port ' + this.port);
        });
        server.on('error', function (e) {
            Logs_1.default.error(`Not connected to express ${e.message}`);
        });
        server.setTimeout(50000000);
    }
}
exports.default = Express;
//# sourceMappingURL=Express.js.map