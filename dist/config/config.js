"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.maxInstances = exports.appUrl = exports.mongoConfig = exports.recaptcha = exports.serverConfig = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
const cfg = dotenv_1.default.config();
const e = process.env;
const serverConfig = {
    port: 3212,
};
exports.serverConfig = serverConfig;
const recaptcha = {
    siteKey: '',
    secretKey: '',
    //siteKey: '6LcTNcMUAAAAALfLcgD0y-A5e6t4vefcFNdeH5ED',
    //secretKey: '6LcTNcMUAAAAADEpxYQ1v20gTFN4h4nHfC1kJqCs',
};
exports.recaptcha = recaptcha;
const mongoConfig = {
    database: 'axie_infinity',
};
exports.mongoConfig = mongoConfig;
const appUrl = '';
exports.appUrl = appUrl;
const maxInstances = 2;
exports.maxInstances = maxInstances;
//# sourceMappingURL=config.js.map