"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rConfig = {
    withCredentials: false,
    timeout: 120000,
    headers: {
        common: {
            Connection: 'keep-alive',
            'Cache-Control': 'max-age=0',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.14 Safari/537.36',
            Accept: '*/*',
            'Sec-Fetch-Site': 'none',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-User': '?1',
            'Sec-Fetch-Dest': 'document',
            'Accept-Encoding': 'gzip, deflate, br',
        },
    },
};
exports.default = rConfig;
//# sourceMappingURL=configRService.js.map