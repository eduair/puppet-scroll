"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeDuplicatesFromArrayByProperty = exports.similarity = exports.access_token_header = exports.session_token_header = exports.findString = exports.unique_hash_header = exports.throwError = exports.getProductId = exports.formatPriceNumber = exports.encodeGetParams = exports.escapeRegExp = exports.deleteDuplicates = exports.isError = exports.bSuccess = exports.bError = exports.makeid = exports.sleep = void 0;
function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
exports.sleep = sleep;
function makeid(length, ids = []) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    const lResult = result.toLowerCase();
    if (ids.length) {
        if (ids.includes(lResult)) {
            return makeid(length, ids);
        }
        else {
            ids.push(lResult);
        }
    }
    return result;
}
exports.makeid = makeid;
function bError(msg) {
    return { error: msg, success: null };
}
exports.bError = bError;
function bSuccess(msg) {
    return { success: msg, error: null };
}
exports.bSuccess = bSuccess;
function isError(res) {
    return res.hasOwnProperty('error');
}
exports.isError = isError;
function deleteDuplicates(a) {
    const uniqueArray = a.filter(function (item, pos) {
        return a.indexOf(item) == pos;
    });
    return uniqueArray;
}
exports.deleteDuplicates = deleteDuplicates;
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
exports.escapeRegExp = escapeRegExp;
/*import * as fs from 'fs';
import axios from 'axios';
export const download_image = (url: string, image_path: string) =>
  axios({
    url,
    responseType: 'stream',
  }).then(
    (response) =>
      new Promise((resolve, reject) => {
        response.data
          .pipe(fs.createWriteStream(image_path))
          .on('finish', () => resolve())
          .on('error', (e) => reject(e));
      })
  );*/
const encodeGetParams = (p) => Object.entries(p)
    .map((kv) => kv.map(encodeURIComponent).join('='))
    .join('&');
exports.encodeGetParams = encodeGetParams;
function formatPriceNumber(value) {
    const val = (value / 1).toFixed(2).replace('.', ',');
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}
exports.formatPriceNumber = formatPriceNumber;
function getProductId(link) {
    const arr = link.split(/\//g);
    return arr[arr.length - 1];
}
exports.getProductId = getProductId;
function throwError(msg) {
    throw new Error(msg);
}
exports.throwError = throwError;
const unique_hash_header = (req) => {
    return req.headers['unique-id-hash'];
};
exports.unique_hash_header = unique_hash_header;
function findString(body, begin, end) {
    if (!body) {
        return false;
    }
    body = body.split(begin);
    body = body[1];
    if (body) {
        body = body.split(end);
        body = body[0];
        return body;
    }
    else {
        return false;
    }
}
exports.findString = findString;
const session_token_header = (req) => {
    return req.headers['session-token'];
};
exports.session_token_header = session_token_header;
const access_token_header = (req) => {
    return req.headers['discord-token'];
};
exports.access_token_header = access_token_header;
function editDistance(s1, s2) {
    s1 = s1.toLowerCase();
    s2 = s2.toLowerCase();
    const costs = [];
    for (let i = 0; i <= s1.length; i++) {
        let lastValue = i;
        for (let j = 0; j <= s2.length; j++) {
            if (i == 0)
                costs[j] = j;
            else {
                if (j > 0) {
                    let newValue = costs[j - 1];
                    if (s1.charAt(i - 1) != s2.charAt(j - 1))
                        newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
                    costs[j - 1] = lastValue;
                    lastValue = newValue;
                }
            }
        }
        if (i > 0)
            costs[s2.length] = lastValue;
    }
    return costs[s2.length];
}
function similarity(s1, s2) {
    let longer = s1;
    let shorter = s2;
    if (s1.length < s2.length) {
        longer = s2;
        shorter = s1;
    }
    const longerLength = longer.length;
    if (longerLength == 0) {
        return 1.0;
    }
    return (longerLength - editDistance(longer, shorter)) / longerLength;
}
exports.similarity = similarity;
const removeDuplicatesFromArrayByProperty = (arr, prop) => {
    if (!arr || typeof arr !== 'object' || arr.length === 0)
        return [];
    return arr.reduce((accumulator, currentValue) => {
        if (!accumulator.find((obj) => obj[prop] === currentValue[prop])) {
            accumulator.push(currentValue);
        }
        return accumulator;
    }, []);
};
exports.removeDuplicatesFromArrayByProperty = removeDuplicatesFromArrayByProperty;
//# sourceMappingURL=Utils.js.map