"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const cheerio_1 = require("cheerio");
const child_process_1 = require("child_process");
const crypto = __importStar(require("crypto"));
const fs_1 = __importDefault(require("fs"));
const ghost_cursor_1 = require("ghost-cursor");
const moment_1 = __importDefault(require("moment"));
const Utils_1 = require("../General/Utils");
const Proxy_1 = __importDefault(require("../Proxy"));
const currencies_1 = __importDefault(require("../currencies"));
const Puppet_1 = require("./Puppet");
const p = new Puppet_1.Puppet();
class PuppetHandler {
    constructor() {
        this.discordRegex = 'discord.gg/[A-Z,a-z,0-9]{7,12}';
        this.telegramRegex = 't.me/[A-Z,a-z,0-9]{1,12}';
        this.getDateAgo = (inputString) => {
            var myRegExp = /^(\d+)\+?\s(\w+)\sago$/;
            var results = myRegExp.exec(inputString);
            var num = parseInt(results[1]);
            var duration = results[2];
            const now = (0, moment_1.default)();
            const d = now.subtract(num, duration).toString();
            return d;
        };
        this.cookies = [];
        this.getSingleProduct = async (url, newProxy, cookies, page, att = 0, pr = null) => {
            const resProduct = await this.getPageBrowser(url)
                .then((res) => {
                const $ = (0, cheerio_1.load)(res.data);
                let gateways = [];
                if (res.data.includes('c')) {
                    gateways.push('paypal');
                }
                if (res.data.includes("'payment_method', 'COINBASE'")) {
                    gateways.push('coinbase');
                }
                if (res.data.includes("'payment_method', 'CASHAPP'")) {
                    gateways.push('cash_app');
                }
                if (res.data.includes("'payment_method', 'VENMO'")) {
                    gateways.push('venmo');
                }
                const name = $('#listing-title').text().trim();
                const image = $('img.object-contain').eq(0).attr('src');
                let description = $('.prose').eq(0).html();
                if (description)
                    description = description.trim();
                let price = $('.flex.flex-row.justify-center.text-2xl.font-extrabold').text();
                let currency = 'USD';
                let priceNumber = 0;
                if (price) {
                    if (price.includes('/')) {
                        price = price.split('/')[0];
                    }
                    // eslint-disable-next-line no-useless-escape
                    currency = price.replace(/[0-9,\.]/g, '').trim();
                    priceNumber = parseFloat(price.replace(currency, '').trim());
                    if (currencies_1.default[currency]) {
                        currency = currencies_1.default[currency];
                    }
                }
                if (isNaN(priceNumber))
                    priceNumber = 0;
                const stockHTML = $('.ml-3.text-slate-600')
                    .eq(0)
                    .text()
                    .replace('in stock', '');
                const stock = stockHTML.trim();
                let quantity_min = parseInt($('[name="purchase_quantity"]').attr('min'));
                if (isNaN(quantity_min))
                    quantity_min = 1;
                let quantity_max = parseInt($('[name="purchase_quantity"]').attr('max'));
                if (isNaN(quantity_max))
                    quantity_max = 1;
                $('.text-xs.font-semibold.mt-1').each((idx, el) => {
                    const link = $(el).text().toLowerCase();
                    if (link && link.includes('pay with')) {
                        const gateway = link.replace('pay with ', '').trim();
                        gateways.push(gateway);
                    }
                });
                let stockNumber = stock === '∞' ? -1 : parseInt(stock);
                if (isNaN(stockNumber)) {
                    stockNumber = -1;
                }
                const totalFeedbacks = $('[data-tab-name="reviews"] > div').length;
                let hash = crypto.createHash('md5').update(url).digest('hex');
                const feedbacks = $('[data-tab-name="reviews"] > div')
                    .map((index, el) => {
                    const pos = totalFeedbacks - index - 1;
                    let dateAgo = $(el).find('.italic.text-xs.text-right').text();
                    if (dateAgo)
                        dateAgo = dateAgo.trim();
                    //let reply = $(el).find("p").eq(0).text().trim();
                    //if (reply === "No reply from store yet.") reply = "";
                    let reply = '';
                    let message = $(el)
                        .find('.italic.text-sm.py-4.px-2')
                        .eq(0)
                        .text()
                        .trim();
                    if (message === 'Automatic feedback after 7 days')
                        message = '';
                    return {
                        product_id: hash,
                        message: message,
                        reply: reply,
                        score: $(el).find('span svg.text-amber-400').length,
                        date_ago: dateAgo,
                        created_at: this.getDateAgo(dateAgo),
                    };
                })
                    .get()
                    .reverse();
                function cl(priceString) {
                    if (!priceString)
                        return '';
                    // Remove any non-numeric characters except for the decimal point
                    const cleanedString = priceString.replace(/[^0-9.]/g, '');
                    return cleanedString;
                }
                if (pr) {
                    const finalPrice = cl(pr.price) || cl(pr.min_price) || cl(pr.max_price);
                    if (!isNaN(parseFloat(finalPrice))) {
                        priceNumber = parseFloat(finalPrice);
                    }
                    if (pr.total_stock)
                        stockNumber = pr.total_stock;
                }
                return {
                    link: url,
                    feedbacks,
                    gateways,
                    name,
                    image: image,
                    description,
                    currency,
                    price: priceNumber,
                    stock: stockNumber,
                    quantity_min,
                    quantity_max,
                };
            })
                .catch(async (e) => {
                console.error(e);
                console.log(e.message);
                if (e.response &&
                    (e.response.statusCode == 404 || e.response.statusCode == 422)) {
                    throw new Error('Disable-404');
                }
                // Try again.
                att++;
                console.log('Att', att, url);
                if (att > 5) {
                    return null;
                }
                await (0, Utils_1.sleep)(1000);
                return this.getSingleProduct(url, newProxy, cookies, page, att, pr);
            });
            return resProduct;
        };
        this.processResponse = async (response, newProxy, cookies, page, productJson) => {
            var _a, _b;
            const shopLink = response.body.url;
            const store = (0, Utils_1.findString)(shopLink, '://', '.sell.app');
            const res = {
                body: response.body.html,
                //products: response.body.products,
                products: [],
            };
            const result = {
                custom_domain: '',
                groups: {},
                avatar: '',
                group_links: [],
                username: store,
                link: shopLink,
                products: [],
                discord_link: '',
                telegram_link: '',
                feedback: {
                    positive: 0,
                    neutral: 0,
                    negative: 0,
                },
                message: '',
            };
            const shopUser = res.body;
            const $ = (0, cheerio_1.load)(shopUser);
            const products = $('.mx-auto a')
                .map((i, el) => {
                const url = $(el).attr('href');
                if (url.includes('product')) {
                    return { url };
                }
            })
                .get();
            res.products = products;
            result.avatar = $('.rounded-full,.rounded-user-large').eq(0).attr('src');
            if (!result.avatar || !result.avatar.includes('http')) {
                result.avatar = 'https://' + store + '.sell.app' + result.avatar;
            }
            let productLinks = res.products
                .filter((el) => el.url)
                .map((el) => el.url.toLowerCase());
            let jsonProduct = {};
            if (productJson === null || productJson === void 0 ? void 0 : productJson.length) {
                productLinks = [];
                for (let item of productJson) {
                    if (item.products) {
                        for (let product of item.products) {
                            jsonProduct[product.url] = product;
                            productLinks.push(product.url);
                        }
                    }
                    else if ((_a = item === null || item === void 0 ? void 0 : item.value) === null || _a === void 0 ? void 0 : _a.url) {
                        const thisUrl = (_b = item === null || item === void 0 ? void 0 : item.value) === null || _b === void 0 ? void 0 : _b.url;
                        jsonProduct[thisUrl] = item.value;
                        productLinks.push(thisUrl);
                    }
                }
            }
            let positive = parseInt($('.span.font-bold.size-base.whitespace-pre-wrap').eq(0).text());
            if (isNaN(positive))
                positive = 0;
            let neutral = parseInt($('.span.font-bold.size-base.whitespace-pre-wrap').eq(1).text());
            if (isNaN(neutral))
                neutral = 0;
            let negative = parseInt($('.span.font-bold.size-base.whitespace-pre-wrap').eq(2).text());
            if (isNaN(negative))
                negative = 0;
            result.feedback = {
                positive: 0,
                neutral: 0,
                negative: 0,
            };
            $('[data-tab-name="feedback"] > div > div').each((idx, el) => {
                const stars = $(el).find('.text-amber-400').length;
                if (stars >= 4) {
                    result.feedback.positive++;
                }
                else if (stars === 3) {
                    result.feedback.neutral++;
                }
                else {
                    result.feedback.negative++;
                }
            });
            $('a.rounded-full').each((idx, el) => {
                const link = $(el).attr('href');
                if (link.includes('discord.gg')) {
                    result.discord_link = link;
                }
                if (link.includes('t.me')) {
                    result.telegram_link = link;
                }
            });
            $('.h-10.w-10.flex.items-center.justify-center').each((idx, el) => {
                const link = $(el).attr('href');
                if (link.includes('discord.gg')) {
                    result.discord_link = link;
                }
                if (link.includes('t.me')) {
                    result.telegram_link = link;
                }
            });
            const message = $('.leading-5').text().trim();
            if (message) {
                result.message = message;
                const telegram_link = this.extractLink(this.telegramRegex, message);
                const discord_link = this.extractLink(this.discordRegex, message);
                if (telegram_link) {
                    result.telegram_link = telegram_link;
                }
                if (discord_link) {
                    result.discord_link = discord_link;
                }
            }
            const group_links = response.body.html_groups;
            result.group_links = [];
            result.groups = {};
            group_links.forEach((el) => {
                const $ = (0, cheerio_1.load)(el);
                const items = el.match(/window\.open\(.{10,200}\)/g);
                const title = $('h3').text().trim();
                if (!result.groups[title])
                    result.groups[title] = [];
                if (items && items.length) {
                    items.forEach((item) => {
                        console.log('item', item);
                        const x = (0, Utils_1.findString)(item, "window.open('", "'");
                        if (x) {
                            const item = x.toLowerCase();
                            if (!result.group_links.includes(item)) {
                                result.group_links.push(item);
                                result.groups[title].push(item);
                            }
                        }
                    });
                }
            });
            if (result.group_links.length > 0) {
                productLinks.push(...result.group_links);
            }
            console.log('total products', productLinks.length);
            if (productLinks && productLinks.length) {
                let idx = 1;
                for (let link of productLinks) {
                    const productTimeout = setTimeout(() => {
                        console.log('Timeout', idx, productLinks.length);
                        process.exit(1);
                    }, 30000);
                    console.log('Pos', idx, productLinks.length, link);
                    if (link.includes('/product')) {
                        const pr = jsonProduct[link];
                        link = 'https://' + store + '.sell.app' + link;
                        const product = await this.getSingleProduct(link, newProxy, cookies, page, 0, pr);
                        if (product)
                            result.products.push(product);
                    }
                    clearTimeout(productTimeout);
                    idx++;
                }
            }
            if ($('.h-10').eq(0)) {
                //result.custom_domain = $('ga').eq(0).attr('href');
                result.custom_domain = '';
            }
            return result;
        };
        this.getProductsSellPass = async (product_data, domain) => {
            let products = [];
            for (let product of product_data) {
                const path = product.path;
                if (product.group) {
                    const listing = product.group.listings;
                    for (let product2 of listing) {
                        const path = product2.path;
                        const url = `https://api.sllpss.com/internal/public/shops/${encodeURIComponent(domain)}/listings/${encodeURIComponent(path)}`;
                        console.log('URL', url);
                        const { data } = await axios_1.default.get(url).catch((e) => {
                            console.log('Error', e.message);
                            return { data: '' };
                        });
                        if (data) {
                            products.push(data.data);
                        }
                    }
                }
                else {
                    const url = `https://api.sllpss.com/internal/public/shops/${encodeURIComponent(domain)}/listings/${encodeURIComponent(path)}`;
                    console.log('URL', url);
                    const { data } = await axios_1.default.get(url);
                    products.push(data.data);
                }
            }
            return products;
        };
        this.getSellPassURL = async (url, res, verify = false) => {
            const domain = url.replace('https://', '');
            const shop_data = await axios_1.default
                .get('https://api.sllpss.com/internal/public/shops/' +
                encodeURI(domain) +
                '/main')
                .catch((e) => {
                return { err: e.message, status: e.response.status };
            });
            if (shop_data.err) {
                res.status(shop_data.status);
                res.json({ error: true, status: shop_data.status });
                return;
            }
            if (verify && shop_data.data) {
                res.json({ success: true });
                return;
            }
            const url_products = 'https://api.sllpss.com/internal/public/shops/' +
                encodeURI(domain) +
                '/listings';
            const products_data = await axios_1.default.get(url_products).then((a) => a.data);
            const feedback_data = await axios_1.default
                .get('https://api.sllpss.com/internal/public/shops/' +
                encodeURI(domain) +
                '/feedbacks')
                .catch((e) => {
                return {
                    data: {
                        data: [],
                    },
                };
            });
            const response = {
                products: await this.getProductsSellPass(products_data.data.listings, domain),
                shop: shop_data.data.data,
                feedbacks: feedback_data.data.data.feedbacks,
            };
            if (res) {
                res.json(response);
                return;
            }
            return response;
        };
        this.getSellPass = async (req, res) => {
            const puppetInstance = new Puppet_1.Puppet();
            const url = req.body.url;
            const verify = req.body.validate;
            const data = await this.getSellPassURL(url, res, verify);
            return data;
        };
        this.closeBrowser = async () => {
            // Close browser.
            await p.closeBrowser();
        };
        this.getAtShop = async (shop, proxy_n) => {
            console.time('atshop_single');
            //const py = exec('npm run atshop');
            console.log('shop', shop);
            const service = './dist/atshop_get.js';
            const py = (0, child_process_1.spawn)('node', [service, shop], { windowsHide: true });
            py.stdout.pipe(process.stdout);
            py.stderr.on('data', (data) => {
                console.log('Err', data);
            });
            const storeJson = await new Promise((resolve) => {
                py.stdout.on('end', function () {
                    try {
                        const path = `atshop-${shop}.json`;
                        const s = fs_1.default.readFileSync(path, 'utf8');
                        fs_1.default.unlinkSync(path);
                        const store = JSON.parse(s);
                        return resolve(store);
                    }
                    catch (e) {
                        return resolve({ error: e.message });
                    }
                });
            });
            console.timeEnd('atshop_single');
            return storeJson;
        };
        this.getSellixApi = async (url, proxy_n) => {
            if (url.includes('api-internal.sellix.io')) {
                console.log('Is api url', url);
                const response = await axios_1.default
                    .get(url)
                    .then((res) => res.data)
                    .catch((e) => {
                    return { error: e.message, status: e.response.status };
                });
                return response;
            }
            const puppetInstance = new Puppet_1.Puppet();
            try {
                let browserStep = null;
                let proxy = null;
                if (proxy_n && !isNaN(proxy_n)) {
                    proxy = Proxy_1.default.getProxy(proxy_n);
                    console.log('Proxy', proxy, url);
                }
                else {
                    console.log('No Proxy', url);
                }
                browserStep = await puppetInstance.startBrowserSellix(proxy);
                if (browserStep && browserStep.error)
                    return this.banned(browserStep.error);
                const pages = await puppetInstance.browser.pages();
                const page = pages[0];
                await page.goto(url, { waitUntil: 'networkidle0', timeout: 60000 });
                if (url.includes('api-internal.sellix.io')) {
                    let html = await page.evaluate(() => document.documentElement.innerText);
                    page.removeAllListeners();
                    await puppetInstance.closeBrowser().catch((e) => console.log(e));
                    return JSON.parse(html);
                }
                let element = await page.$('.sellix-block');
                const page_title = await page.title();
                if (page_title.includes('terminated') ||
                    page_title.includes('Shop Banned')) {
                    (0, Utils_1.throwError)('terminated');
                }
                if (page_title.includes('not found') ||
                    page_title.includes('Missing Theme')) {
                    (0, Utils_1.throwError)('not_found');
                }
                console.log(page_title);
                if (!element) {
                    await page.screenshot({ path: 'sellix_error.png' });
                    if (page_title === 'Access denied') {
                        throw new Error(page_title);
                    }
                    await page.waitForSelector('.sellix-block').catch(async (e) => {
                        console.log('Pre not found');
                        console.error(e);
                    });
                    element = await page.$('.sellix-block');
                }
                let html = '';
                if (element) {
                    html = await page.evaluate(() => document.documentElement.outerHTML);
                }
                page.removeAllListeners();
                await puppetInstance.closeBrowser().catch((e) => console.log(e));
                return this.processSellixHTML(html);
            }
            catch (e) {
                await puppetInstance.closeBrowser().catch((e) => console.log(e));
                if (e.message === 'not_found') {
                    return { error: 'not_found', status: 404 };
                }
                return { error: e.message };
            }
        };
        this.getAtshop = async (req, res) => {
            try {
                console.log('Request received', req.body);
                let shop = req.body.shop;
                if (!shop)
                    shop = 'whipsolutions';
                const proxy_n = parseInt(req.body.proxy);
                const html = await this.getAtShop(shop, proxy_n);
                return res.json(html);
            }
            catch (e) {
                console.error(e);
                return res.json({ error: e.message }).status(500);
            }
        };
        this.getSellix = async (req, res) => {
            try {
                console.log('Request received', req.body);
                let url = req.body.url;
                if (!url)
                    url = 'https://checkleakedcc.mysellix.io/';
                const proxy_n = parseInt(req.body.proxy);
                const html = await this.getSellixApi(url, proxy_n);
                return res.json(html);
            }
            catch (e) {
                console.error(e);
                return res.json({ error: e.message }).status(500);
            }
        };
        this.getSingleProductReq = async (req, res) => {
            const url = req.body.url ||
                'https://lawnstore.sell.app/product/disney-premium-3-months-warranty';
            const proxy = req.body.proxy;
            const puppetInstance = new Puppet_1.Puppet();
            let browserStep = null;
            let final_r;
            try {
                browserStep = await puppetInstance.startBrowser(proxy);
                if (browserStep && browserStep.error)
                    return this.banned(browserStep.error);
                //await puppetInstance.setBypassMethods();
                await puppetInstance.createIncognito();
                await puppetInstance.createPageIncognito();
                const page = puppetInstance.page;
                final_r = await this.getSingleProduct(url, proxy, [], page);
            }
            catch (e) {
                try {
                    await puppetInstance.closeBrowser().catch((e) => console.log(e));
                }
                catch (e) {
                    console.error(e);
                    console.log('Error closing browser');
                }
            }
            return final_r;
        };
        this.clickBtn = async (page, att = 0, captchaAtt = 0, cursor = null) => {
            const title = await page.title();
            console.log('Title Page', att, title);
            if (title.includes('Turnstile') || title.includes('Terms of Use')) {
                await page.goBack({ waitUntil: 'networkidle2' });
                cursor = (0, ghost_cursor_1.createCursor)(page);
            }
            await page.screenshot({ path: 'title_page.png' });
            const element = await page.$('body');
            const html = await page.evaluate((el) => el.innerHTML, element);
            fs_1.default.writeFileSync('title_page.html', html);
            const hasVerifyHumanBtn = await page
                .evaluate(() => {
                let el = document.querySelector('.big-button.pow-button');
                return el ? true : false;
            })
                .catch((e) => {
                return false;
            });
            const hasCaptcha = await page
                .evaluate(() => {
                let el = document.querySelector('.cf-turnstile-wrapper');
                return el ? true : false;
            })
                .catch((e) => {
                return false;
            });
            console.log('hasVerifyHumanBtn', hasVerifyHumanBtn);
            console.log('hasCaptcha', hasCaptcha);
            if (!cursor) {
                cursor = (0, ghost_cursor_1.createCursor)(page);
            }
            if (hasVerifyHumanBtn) {
                await cursor.click('.big-button.pow-button').catch((e) => null);
                await (0, Utils_1.sleep)(1500);
            }
            else if (hasCaptcha) {
                captchaAtt++;
                const acceptBtn = await this.hasCaptchaHandler(page);
                if (acceptBtn) {
                    console.log('Captcha button found, resolve captcha');
                    captchaAtt++;
                    await acceptBtn.click().catch((e) => console.error(e));
                    await (0, Utils_1.sleep)(1000);
                    await acceptBtn.click().catch((e) => console.error(e));
                }
                await (0, Utils_1.sleep)(1500);
                console.log('Captcha att', captchaAtt);
                if (captchaAtt > 3) {
                    console.log('More than 3 captcha attempts');
                }
            }
            if (att > 10)
                return;
            setTimeout(() => {
                att++;
                this.clickBtn(page, att, captchaAtt, cursor);
            }, 2000);
        };
        this.getSellAppPage = async (url, validate, proxy, timer) => {
            let title = '';
            let html = '';
            let products = [];
            let cookies = [];
            let r_final = {
                url: '',
                html: '',
                products: [],
                html_groups: [],
                status: 200,
            };
            console.log('Url', url);
            let extra_r = {};
            let newProxy = '';
            if (url) {
                let page = null;
                try {
                    const puppetInstance = p;
                    if (!p.browser) {
                        const proxySp = Proxy_1.default.getProxy(1);
                        await p.startBrowser(proxySp);
                    }
                    if (!process.env.proxyless) {
                        newProxy = proxy || process.env.proxy || '';
                    }
                    //await puppetInstance.setBypassMethods();
                    await puppetInstance.createPage(true);
                    page = puppetInstance.page;
                    //p.blockRequests(page);
                    let html_groups = [];
                    // page.on("response", async (response) => {
                    //   if (response) {
                    //     const url = response.url();
                    //     const check = url.includes("store.show");
                    //     if (check) {
                    //       console.log("store.show");
                    //       const text: any = await response.json();
                    //       const arr: any = Object.values(text.effects.returns);
                    //       const thisProducts: any[] = arr[0];
                    //       products.push(...thisProducts);
                    //     }
                    //     const group_check = url.includes("store.groups.show");
                    //     if (group_check) {
                    //       console.log("url", url);
                    //       try {
                    //         const text: any = await response.json();
                    //         const raw_html = text.effects.html;
                    //         html_groups.push(raw_html);
                    //       } catch (e) {
                    //         console.log("Error", e);
                    //       }
                    //     }
                    //   }
                    // });
                    await page
                        .goto(url, { waitUntil: 'domcontentloaded', timeout: 120000 })
                        .catch(async () => {
                        console.log('Timeout navigation', url);
                        p.saveWs('');
                        await p.browser.close();
                        const proxySp = Proxy_1.default.getProxy(1);
                        console.log('Proxy', proxySp);
                        await p.startBrowser(proxySp);
                        (0, Utils_1.throwError)('Timeout');
                    });
                    title = await page.title();
                    console.log('Title', title);
                    if (title === 'Access denied' || title.includes('Attention Required')) {
                        // Restart browser as access was denied, update proxy.
                        p.saveWs('');
                        await p.browser.close();
                        const proxySp = Proxy_1.default.getProxy(1);
                        console.log('Proxy', proxySp);
                        await p.startBrowser(proxySp);
                        (0, Utils_1.throwError)('Access Denied');
                    }
                    else if (title.includes('Just a moment')) {
                        puppetInstance.disconnectBrowser();
                        console.log('Wait 10 seconds before retrying');
                        await (0, Utils_1.sleep)(10000);
                        return this.getSellAppPage(url, validate, proxy, timer);
                    }
                    clearTimeout(timer);
                    const newTimer = setTimeout(() => {
                        console.log('Timeout');
                        process.exit(1);
                    }, 60000);
                    // const f = (await page.$('.contents')) !== null;
                    // // Check for content.
                    // if (!f) {
                    // 	//this.clickBtn(page);
                    // 	const res = await page
                    // 		.waitForSelector('.contents', { timeout: 60000 })
                    // 		.then(() => {
                    // 			return { error: false };
                    // 		})
                    // 		.catch(() => {
                    // 			return { error: true };
                    // 		});
                    // 	if (res.error) {
                    // 		await page.screenshot({ path: 'cloudflare_ban.png' });
                    // 		console.log('Error', res.error);
                    // 		throwError('Cloudflare ban');
                    // 	}
                    // }
                    this.saveCookies(page);
                    const timeout = 7000;
                    console.log('Scroll to bottom');
                    html = await puppetInstance.scrollToBottom(page, timeout);
                    console.log('Total Groups');
                    // const total_groups: any = await page.evaluate(async () => {
                    //   function sleep(ms: number) {
                    //     return new Promise((resolve) => setTimeout(resolve, ms));
                    //   }
                    //   const list = document.querySelectorAll("button.group") as NodeListOf<HTMLElement>;
                    //   let total = 0;
                    //   if (list && list.length) {
                    //     for (let element of list) {
                    //       const txtEl = element.querySelector("p.relative.text-xs") as HTMLElement;
                    //       let chk = false;
                    //       if (txtEl) {
                    //         const txt = txtEl.innerText;
                    //         console.log("txt", txt);
                    //         chk = txt.includes("Group");
                    //       }
                    //       if (chk) {
                    //         element.click();
                    //         total++;
                    //         await sleep(1000);
                    //       }
                    //     }
                    //   }
                    //   return await new Promise((resolve) => resolve(total));
                    // });
                    // console.log("Total groups", total_groups);
                    // if (total_groups) {
                    //   await new Promise(async (resolve) => {
                    //     let att = 0;
                    //     console.log("Check", total_groups, html_groups.length);
                    //     while (total_groups > html_groups.length && att < 10) {
                    //       await sleep(1000);
                    //       att++;
                    //     }
                    //     resolve(true);
                    //   });
                    //   console.log("Finish total groups", total_groups, html_groups.length);
                    // }
                    const productJson = await page.evaluate(async (url) => {
                        const usernameFromUrl = url
                            .split('.sell.app')[0]
                            .replace('https://', '');
                        const urlData = url + '/api/products/' + usernameFromUrl;
                        const data = await fetch(urlData)
                            .then((res) => res.json())
                            .catch((e) => {
                            console.error(e);
                            return [];
                        });
                        return data;
                    }, url);
                    cookies = await puppetInstance.getCookies();
                    fs_1.default.writeFileSync('cookies.json', JSON.stringify(cookies));
                    clearTimeout(newTimer);
                    try {
                        console.log('Close page');
                        r_final = { url, html, products, html_groups, status: 200 };
                        if (!validate) {
                            extra_r = await this.processResponse({ body: r_final }, newProxy, cookies, page, productJson);
                        }
                    }
                    catch (e) {
                        console.error(e);
                        console.log('Error closing browser');
                    }
                }
                catch (e) {
                    await puppetInstance.closeBrowser().catch((e) => console.log(e));
                    console.error(e);
                    return {
                        title: title,
                        products: [],
                        error: true,
                        status: 500,
                    };
                }
            }
            if (html &&
                html.includes('<h1 class="text-xl font-bold">404 Not Found</h1>')) {
                r_final = { url, html, products, html_groups: [], status: 422 };
                return r_final;
            }
            return Object.assign(Object.assign({ title }, extra_r), { status: 200 });
        };
        this.getPageRequest = async (req, res) => {
            const url = req.body.url;
            const validate = req.body.validate === true;
            const proxy = req.body.proxy;
            const response = await this.getSellAppPage(url, validate, proxy, null);
            res.status(response.status);
            return response;
        };
        console.log('Init Puppet');
    }
    async startBrowser() {
        return p.startBrowser('');
    }
    extractLink(regexPattern, item) {
        let link = '';
        const items = item.match(new RegExp(regexPattern, 'gi'));
        if (items && items.length) {
            link = 'https://' + items[0];
        }
        return link;
    }
    banned(res) {
        return { success: false, error: false, banned: res };
    }
    async getPageBrowser(url) {
        console.log('getPageBrowser', url);
        if (!p.browser) {
            const proxySp = Proxy_1.default.getProxy(1);
            await p.startBrowser(proxySp);
        }
        await p.createPage(true);
        const page = p.page;
        await page
            .goto(url, { waitUntil: 'load', timeout: 10000 })
            .catch((e) => console.log(e));
        let title = await page.title();
        console.log('Title', title);
        if (title === 'Access denied' || title.includes('Attention Required')) {
            // Restart browser as access was denied, update proxy.
            p.saveWs('');
            await p.browser.close();
            const proxySp = Proxy_1.default.getProxy(1);
            console.log('Proxy', proxySp);
            await p.startBrowser(proxySp);
            (0, Utils_1.throwError)('Access Denied');
        }
        else if (title.includes('Just a moment')) {
            p.disconnectBrowser();
            console.log('Wait 10 seconds before retrying');
            await (0, Utils_1.sleep)(10000);
            return this.getPageBrowser(url);
        }
        const html = await page.evaluate(() => document.documentElement.outerHTML);
        return {
            data: html,
        };
    }
    async getPage(urlSearch, page) {
        let html = await page.evaluate((urlSearch) => {
            return fetch(urlSearch)
                .then(function (response) {
                return {
                    text: response.text(),
                    status: response.status,
                };
            })
                .then(function (res) {
                if (res.status === 200)
                    return res.text;
                return '';
            })
                .catch(function (err) {
                console.error(err);
                return '';
            });
        }, urlSearch);
        return {
            data: html,
        };
    }
    getCookies() {
        if (this.cookies.length) {
            return this.cookies;
        }
        try {
            const cookies = fs_1.default.readFileSync('cookies.json', 'utf8');
            return JSON.parse(cookies);
        }
        catch (e) {
            return [];
        }
    }
    async setCookiesPage(page) {
        const cookies = this.getCookies();
        for (let index = 0; index < cookies.length; index++) {
            const cookie = cookies[index];
            await page.setCookie(cookie).catch((e) => {
                return null;
            });
        }
    }
    async saveCookies(page) {
        this.cookies = await page.cookies();
        const strCookies = JSON.stringify(this.cookies);
        fs_1.default.writeFileSync('cookies.json', strCookies);
    }
    processSellixHTML(html) {
        if (!html)
            return { error: 'no_html', status: 500 };
        const start = 'window.__RENDER_CONTEXT__ = ';
        const end = 'window.RECAPTCHA_PUBLIC_KEY';
        let str = (0, Utils_1.findString)(html, start, end);
        str = str.trim().slice(0, -1);
        const json = JSON.parse(str);
        if (!json)
            return { error: 'no_html', status: 500 };
        return Object.assign(Object.assign({}, json.common.shopInfo), { status: 200 });
    }
    async hasCaptchaHandler(page) {
        if ((await page.$('.cf-turnstile-wrapper')) === null) {
            await page.waitForSelector('.cf-turnstile-wrapper').catch((e) => {
                console.error(e);
            });
        }
        if ((await page.$('.cf-turnstile-wrapper')) !== null) {
            let frame = page
                .frames()
                .find((f) => f.url().startsWith('https://challenges.cloudflare.com/'));
            if (!frame) {
                await (0, Utils_1.sleep)(1500);
                frame = page
                    .frames()
                    .find((f) => f.url().startsWith('https://challenges.cloudflare.com/'));
            }
            if (frame) {
                console.log('Frame', frame !== undefined);
                let acceptBtn = await frame.$(`.ctp-checkbox-label .mark`);
                if (!acceptBtn) {
                    acceptBtn = await frame
                        .waitForSelector(`.ctp-checkbox-label .mark`, { timeout: 10000 })
                        .catch((e) => null);
                }
                if (acceptBtn) {
                    await (0, Utils_1.sleep)(1000);
                    acceptBtn = await frame.$(`.ctp-checkbox-label .mark`);
                    return acceptBtn;
                }
            }
        }
        return false;
    }
}
const puppetInstance = new PuppetHandler();
exports.default = puppetInstance;
//# sourceMappingURL=index.js.map