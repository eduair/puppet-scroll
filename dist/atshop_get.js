"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const getAtShop_1 = require("./AtShop/getAtShop");
async function main() {
    const store = process.argv[2];
    const res = await (0, getAtShop_1.getAtShop)(store);
    const path = `atshop-${store}.json`;
    fs_1.default.writeFileSync(path, JSON.stringify(res));
    process.exit(1);
}
main();
//# sourceMappingURL=atshop_get.js.map