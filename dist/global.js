"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildError = exports.buildSuccess = void 0;
function buildSuccess(string) {
    return { success: string, error: false };
}
exports.buildSuccess = buildSuccess;
function buildError(string) {
    return { error: string, success: false };
}
exports.buildError = buildError;
//# sourceMappingURL=global.js.map