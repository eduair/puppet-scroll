"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
process.setMaxListeners(0);
const config_1 = require("./config/config");
const Express_1 = __importDefault(require("./Express/Express"));
const Ghunt_1 = require("./Ghunt");
const server = new Express_1.default(8721, '/', config_1.recaptcha);
class Main {
    constructor() {
        console.log('Ghunt Server');
        this.init();
    }
    init() {
        server.postJsonAlt2('ghunt', Ghunt_1.GHuntReq);
        server.postJsonAlt2('ghunt/ping', Ghunt_1.GHuntReqPing);
    }
}
async function start() {
    new Main();
}
start();
//# sourceMappingURL=ghuntServer.js.map