"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Puppet_1 = __importDefault(require("./Puppet"));
async function main() {
    const link = 'https://timba.sellpass.io';
    const response = await Puppet_1.default.getSellPassURL(link);
    console.log('response', response);
}
main();
//# sourceMappingURL=sellpass.js.map