"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Puppet_1 = __importDefault(require("./Puppet"));
const SellApp_1 = require("./SellApp");
async function main() {
    async function syncLast() {
        try {
            const timer = setTimeout(() => {
                console.log('Timeout, restart process');
                process.exit(1);
            }, 60000);
            const { entry, url } = await SellApp_1.sellApp.getStore();
            console.log('Data', entry, url);
            // 5 minutes to try to obtain the data
            const response = await Puppet_1.default.getSellAppPage(url, false, '', timer);
            if (response.status !== 500) {
                console.log('Valid page', url);
                response.username = entry.user;
                console.log('To upload', response);
                const uploadResponse = await SellApp_1.sellApp.uploadStore(entry, response);
                console.log(uploadResponse);
            }
            else {
                console.log('Error getting page info', response);
                process.exit(1);
            }
        }
        catch (e) {
            console.error(e);
            process.exit(1);
        }
        setTimeout(() => {
            syncLast();
        }, 1500);
    }
    syncLast();
}
main();
//# sourceMappingURL=auto.js.map