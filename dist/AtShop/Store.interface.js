"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StoresEnum = exports.StoreErrors = void 0;
var StoreErrors;
(function (StoreErrors) {
    StoreErrors["notFound"] = "Shop not found";
    StoreErrors["invalidLink"] = "Invalid Shop Link";
    StoreErrors["notAvailable"] = "Shop not available";
})(StoreErrors || (exports.StoreErrors = StoreErrors = {}));
var FinalProductUpdateType;
(function (FinalProductUpdateType) {
    FinalProductUpdateType[FinalProductUpdateType["new_product"] = 0] = "new_product";
    FinalProductUpdateType[FinalProductUpdateType["up_stock"] = 1] = "up_stock";
    FinalProductUpdateType[FinalProductUpdateType["down_stock"] = 2] = "down_stock";
    FinalProductUpdateType[FinalProductUpdateType["up_price"] = 3] = "up_price";
    FinalProductUpdateType[FinalProductUpdateType["down_price"] = 4] = "down_price";
})(FinalProductUpdateType || (FinalProductUpdateType = {}));
var StoresEnum;
(function (StoresEnum) {
    StoresEnum["Sellix"] = "sellix";
    StoresEnum["SellApp"] = "sell_app";
    StoresEnum["Shoppy"] = "shoppy";
    StoresEnum["Selly"] = "selly";
    StoresEnum["AtShop"] = "atshop";
    StoresEnum["AutoBuy"] = "autobuy";
    StoresEnum["SellPass"] = "sell_pass";
    StoresEnum["Invicta"] = "invicta";
    StoresEnum["Sellup"] = "sellup";
})(StoresEnum || (exports.StoresEnum = StoresEnum = {}));
//# sourceMappingURL=Store.interface.js.map