"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AtShopCollections = void 0;
var AtShopCollections;
(function (AtShopCollections) {
    AtShopCollections["kadira_settings"] = "kadira_settings";
    AtShopCollections["shops"] = "shops";
    AtShopCollections["shop_products"] = "shop.products";
    AtShopCollections["shop_order_feedback"] = "shop.order.feedback";
    AtShopCollections["shop_product_groups"] = "shop.products.group";
    AtShopCollections["shop_categories"] = "shop.categories";
    AtShopCollections["shop_gateways"] = "shop.gateways";
})(AtShopCollections || (exports.AtShopCollections = AtShopCollections = {}));
//# sourceMappingURL=AtShop.interfaces.js.map