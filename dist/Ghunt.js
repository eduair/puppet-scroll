"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GHuntReq = exports.GHuntReqPing = exports.GHunt = void 0;
const child_process_1 = require("child_process");
const dotenv_1 = __importDefault(require("dotenv"));
const fs_1 = __importDefault(require("fs"));
const node_util_1 = __importDefault(require("node:util"));
dotenv_1.default.config();
const execPromise = node_util_1.default.promisify(child_process_1.exec);
const GHunt = async (email) => {
    let route = 'C:\\Users\\airau\\Desktop\\ghunt-php-api\\main.py';
    if (process.env.ghuntRoute) {
        route = process.env.ghuntRoute;
    }
    const command = `set PYTHONIOENCODING=utf-8 && set PYTHONLEGACYWINDOWSSTDIO=utf-8 && python ${route} email <email> --json ghunt/<email>.json`;
    const cmd = command.replace(/<email>/g, email);
    await execPromise(cmd);
    const filePath = `ghunt/${email}.json`;
    const fileData = fs_1.default.readFileSync(filePath, 'utf-8');
    let json = JSON.parse(fileData);
    fs_1.default.unlinkSync(filePath);
    return json;
};
exports.GHunt = GHunt;
const GHuntReqPing = (req, res) => {
    return res.json({ status: 'ok' });
};
exports.GHuntReqPing = GHuntReqPing;
const GHuntReq = async (req, res) => {
    try {
        const email = req.body.email;
        const json = await (0, exports.GHunt)(email);
        return res.json(json);
    }
    catch (e) {
        return res.json({ error: e.message });
    }
};
exports.GHuntReq = GHuntReq;
//# sourceMappingURL=Ghunt.js.map