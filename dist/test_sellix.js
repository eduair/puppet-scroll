"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Puppet_1 = __importDefault(require("./Puppet"));
async function main() {
    let link = process.argv[3];
    const proxy_n = parseInt(process.argv[4]);
    if (!link)
        link = 'https://flamurmart.mysellix.io/';
    console.log('link', link);
    const res = await Puppet_1.default.getSellixApi(link, proxy_n);
    console.log('Response', res);
}
main();
//# sourceMappingURL=test_sellix.js.map