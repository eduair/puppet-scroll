import pyautogui
import sys
import time
import cv2
import os
import uuid
from locate import locate_image
pyautogui.FAILSAFE = False

# Define the path to the image you want to find (PNG format)
image_path = 'computer/captcha.png'

# Attempt to locate the image on the screen
image_location = locate_image(image_path)

if image_location is not None:
    # Get the center coordinates of the found image
    (click_x, click_y) = image_location

    # Perform a click on the image
    time.sleep(1)
    pyautogui.click(click_x, click_y)
    time.sleep(1)
    pyautogui.moveTo(0, 0)
    print("Image found, click")