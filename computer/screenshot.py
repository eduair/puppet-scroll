import pyautogui

# Capture a screenshot of the entire screen
screenshot = pyautogui.screenshot()

# Save the screenshot to a file (optional)
screenshot.save("screenshot.png")
print("screenshot.png")