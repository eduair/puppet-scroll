const util = require('util');
const exec = util.promisify(require('child_process').exec);

async function main() {
  const cmd = 'python computer/captcha_alt.py'; // Define the command
  try {
    const { stdout, stderr } = await exec(cmd, { windowsHide: true });
    if (stdout) {
      console.log(stdout);
    }
  } catch (error) {
    console.error('Error:', error);
  }
  setTimeout(() => {
    main();
  }, 1000);
}
console.log('Start script clicker captcha');
main();
