import cv2
import pyautogui
import uuid
import numpy as np
import tempfile

def locate_image(path):
    # Load the main image
    main_image = cv2.imread(path)

    # Load the template image (the image you want to find within the main image)

    temp_dir = tempfile.TemporaryDirectory()
    screenshot_path = temp_dir.name + '/' + str(uuid.uuid4()) + '.png';
    screenshot = pyautogui.screenshot()
    screenshot.save(screenshot_path)
    template_image = cv2.imread(screenshot_path)

    # Use template matching to find the template within the main image
    result = cv2.matchTemplate(main_image, template_image, cv2.TM_CCOEFF_NORMED)

    # Specify a threshold to consider a match
    threshold = 0.95  # Adjust this threshold as needed

    # Use np.where to find locations where the result is above the threshold
    locations = np.where(result >= threshold)

    size = locations[0].size

    if size <= 0: 
        return None;

    # Get the location of the best match
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)

    # Get the coordinates of the top-left corner of the match
    h, w, _ = main_image.shape
    center_x = max_loc[0] + w // 2
    center_y = max_loc[1] + h // 2

    return (center_x, center_y)